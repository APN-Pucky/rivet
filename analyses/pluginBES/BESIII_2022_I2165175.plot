BEGIN PLOT /BESIII_2022_I2165175/d01-x01-y01
Title=Cross section for $e^+e^-\to \phi\eta^\prime$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
