BEGIN PLOT /LHCB_2020_I1760788/d01-x01-y01
Title=$\sigma(\Xi_{cc}^{++})\times \text{Br}(\Xi_{cc}^{++}\to\Lambda_c^+K^-\pi^+\pi^+)/\sigma(\Lambda_c^+)$ ($4<p_\perp<15$\,GeV, $2<y<4.5$)
YLabel=Rate [$[10^{-4}$]
XLabel=Lifetime [ps]
XCustomMajorTicks=1. $\tau=0.230$     2.     $\tau=0.256$   3.    $\tau=0.284$ 
END PLOT
