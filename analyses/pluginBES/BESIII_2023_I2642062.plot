BEGIN PLOT /BESIII_2023_I2642062/d01-x01-y01
Title=$\sigma(e^+e^-\to \Lambda\bar{\Lambda})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Lambda\bar{\Lambda})$/pb
ConnectGaps=1
END PLOT
