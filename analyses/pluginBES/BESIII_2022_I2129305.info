Name: BESIII_2022_I2129305
Year: 2022
Summary:  Cross section for $e^+e^-\to \pi^+\pi^- D^+D^-$ between 4.19 and 4.946 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2129305
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2208.00099 [hep-ex]
RunInfo: e+e- to hadrons, KS0 and pi0 should be set stable
NeedCrossSection: yes
Beams:  [e+,e-]
Energies : [4.19, 4.2, 4.21, 4.22, 4.23, 4.237, 4.245, 4.246, 4.26, 4.269, 4.27, 4.28, 4.29, 4.31,
            4.315, 4.34, 4.36, 4.38, 4.39, 4.4, 4.42, 4.44, 4.47, 4.53, 4.575, 4.6, 4.612, 4.62, 4.64,
            4.66, 4.68, 4.7, 4.74, 4.75, 4.78, 4.84, 4.914, 4.916, 4.946]
Options:
 - ENERGY=*
Description:
   'Cross section for $e^+e^-\to \pi^+\pi^- D^+D^-$ between 4.19 and 4.946 GeV. The cross section for the $\psi_3(3842)$ resonant contribution is also measured.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Ablikim:2022ozi
BibTeX: '@article{Ablikim:2022ozi,
    author = "Ablikim, M. and others",
    title = "{Measurement of $e^{+}e^{-}\rightarrow\pi^{+}\pi^{-}D^{+}D^{-}$ cross sections at center-of-mass energies from 4.190 to 4.946 GeV}",
    eprint = "2208.00099",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "7",
    year = "2022"
}
'
