BEGIN PLOT /BESIII_2022_I1989527/d01-x01-y03
Title=$\sigma(e^+e^-\to D^{*+}D^{*-})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^{*+}D^{*-})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2022_I1989527/d02-x01-y03
Title=$\sigma(e^+e^-\to D^\pm D^{*\mp})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^\pm D^{*\mp})$/pb
ConnectGaps=1
END PLOT
