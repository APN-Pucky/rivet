BEGIN PLOT /CLEOC_2008_I784516/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\psi(2S)\to\pi^+\pi^-J/\psi$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2008_I784516/d01-x01-y02
Title=$\pi^0\pi^0$ mass distribution in $\psi(2S)\to\pi^0\pi^0J/\psi$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
