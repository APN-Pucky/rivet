BEGIN PLOT /BESIII_2015_I1376484/d01-x01-y01
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$
XLabel=$X$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}X$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376484/d01-x01-y02
Title=$Y$ distribution for $\eta\to\pi^+\pi^-\pi^0$
XLabel=$Y$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Y$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376484/d02-x01-y01
Title=$Z$ distribution for $\eta\to\pi^0\pi^0\pi^0$
XLabel=$Z$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Z$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376484/d03-x01-y01
Title=$Z$ distribution for $\eta^\prime\to\pi^0\pi^0\pi^0$
XLabel=$Z$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Z$
LogY=0
END PLOT