BEGIN PLOT /ATLAS_2024_I2771257/d04
XLabel=$p_\mathrm{T}(Z)$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [pb/GeV]
Title=$Z(\to\ell\ell) + \geq 1$ $b$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d06
XLabel=$\Delta\tilde{R}_{Zb}$
YLabel=$\mathrm{d}\sigma / \Delta\tilde{R}_{Zb}$ [pb]
Title=$Z(\to\ell\ell) + \geq 1$ $b$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d05
XLabel=Leading $b$-jet $p_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [pb/GeV]
Title=$Z(\to\ell\ell) + \geq 1$ $b$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d09
XLabel=$p_\mathrm{T}(Z)$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [pb/GeV]
Title=$Z(\to\ell\ell) + \geq 1$ $c$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d10
XLabel=Leading $c$-jet $p_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}$ [pb/GeV]
Title=$Z(\to\ell\ell) + \geq 1$ $c$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d11
XLabel=Leading $c$-jet $x_\mathrm{F}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_\mathrm{F}$ [pb]
Title=$Z(\to\ell\ell) + \geq 1$ $c$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d07
XLabel=$\Delta\phi_{bb}$
YLabel=$\mathrm{d}\sigma / \Delta\phi_{bb}$ [pb]
Title=$Z(\to\ell\ell) + \geq 2$ $b$-jet
END PLOT

BEGIN PLOT /ATLAS_2024_I2771257/d08
XLabel=$m_{bb}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}m_{bb}$ [pb/GeV]
Title=$Z(\to\ell\ell) + \geq 2$ $b$-jet
END PLOT
