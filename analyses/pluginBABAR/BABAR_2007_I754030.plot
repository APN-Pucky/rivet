BEGIN PLOT /BABAR_2007_I754030/d01-x01-y01
Title=Helicity angle in $B^+\to\omega K^+$
XLabel=$|\cos\theta|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta|$
LogY=0
END PLOT
