BEGIN PLOT /CLEO_2001_I535113/d01
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I535113/d01-x01-y01
Title=Kaon helicity angle for $B^+\to \psi(2S) K^{*+}(\to K^0_S\pi^+)$
END PLOT
BEGIN PLOT /CLEO_2001_I535113/d01-x01-y02
Title=Kaon helicity angle for $B^+\to \psi(2S) K^{*+}(\to K^+\pi^0)$
END PLOT
BEGIN PLOT /CLEO_2001_I535113/d01-x01-y03
Title=Kaon helicity angle for $B^0\to \psi(2S) K^{*0}(\to K^+\pi^-)$
END PLOT
