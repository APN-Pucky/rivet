BEGIN PLOT /BABAR_2010_I844288/d01-x01-y01
Title=$D\bar{D}$ mass in $\gamma\gamma\to D\bar{D}$
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I844288/d02-x01-y01
Title=$D$ $\cos\theta$ distribution for $Z(3930)$
XLabel=$|\cos\theta|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta|$
LogY=0
END PLOT
