Name: LHCB_2022_I1971920
Year: 2022
Summary: Decay asymmetry in $\Lambda_b^0\to\Lambda^0\gamma$
Experiment: LHCB
Collider: LHC
InspireID: 1971920
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 105 (2022) 5, L051104
RunInfo: Any process producing Lambda_b, original pp
Description:
  'Measurement of the decay asymmetry in $\Lambda_b^0\to\Lambda^0\gamma$ by LHCb'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2021byf
BibTeX: '@article{LHCb:2021byf,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of the photon polarization in $\Lambda_{b}^{0}$ $\to$ $\Lambda$ $\gamma$ decays}",
    eprint = "2111.10194",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCb-PAPER-2021-030, CERN-EP-2021-230",
    doi = "10.1103/PhysRevD.105.L051104",
    journal = "Phys. Rev. D",
    volume = "105",
    number = "5",
    pages = "L051104",
    year = "2022"
}
'
