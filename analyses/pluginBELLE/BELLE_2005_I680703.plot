BEGIN PLOT /BELLE_2005_I680703/d01-x01-y01
Title=$\cos\theta_{\text{tr}}$ for $B\to J/\psi K^*$
XLabel=$\cos\theta_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\text{tr}}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I680703/d01-x01-y02
Title=$\phi_{\text{tr}}$ for $B\to J/\psi K^*$
XLabel=$\phi_{\text{tr}}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_{\text{tr}}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I680703/d01-x01-y03
Title=Kaon helicity angle for $B\to J/\psi K^*$
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
