Name: BABAR_2009_I797507
Year: 2009
Summary: Measurement of $R_b$ for energies between 10.541 and 11.206 GeV
Experiment: BABAR
Collider: PEP-II
InspireID: 797507
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - PRL 102,012001
RunInfo: e+ e- to hadrons and e+ e- to mu+ mu- (for normalization)
NeedCrossSection: no
Beams: [e-, e+]
Energies:  [10.5408, 10.5583, 10.5628, 10.5673, 10.5723, 10.5738, 10.5788, 10.5903,
            10.5983, 10.6018, 10.6063, 10.6118, 10.6128, 10.6178, 10.6273, 10.6308,
            10.6353, 10.6413, 10.6458, 10.6518, 10.6553, 10.6613, 10.6659, 10.6724,
            10.6774, 10.6819, 10.6869, 10.6884, 10.6964, 10.7009, 10.7044, 10.7099,
            10.7164, 10.7189, 10.7249, 10.7294, 10.7344, 10.7409, 10.7449, 10.7459,
            10.7539, 10.7589, 10.7639, 10.7679, 10.7739, 10.7789, 10.7839, 10.7889,
            10.7949, 10.7979, 10.7999, 10.8075, 10.813, 10.8175, 10.822, 10.8275,
            10.832, 10.8375, 10.8415, 10.8455, 10.86, 10.8605, 10.8645, 10.87,
            10.876, 10.881, 10.886, 10.888, 10.894, 10.8985, 10.9055, 10.9095,
            10.911, 10.9135, 10.9185, 10.9235, 10.9315, 10.9365, 10.9386, 10.9456,
            10.9501, 10.9536, 10.9576, 10.9641, 10.9686, 10.9726, 10.9791, 10.9836,
            10.9901, 10.9936, 10.9991, 11.0041, 11.0086, 11.0151, 11.0191, 11.0241,
            11.0266, 11.0331, 11.0401, 11.0446, 11.0491, 11.0531, 11.0581, 11.0636,
            11.0686, 11.0721, 11.0787, 11.0827, 11.0877, 11.0932, 11.0982, 11.1012,
            11.1077, 11.1092, 11.1112, 11.1162, 11.1217, 11.1262, 11.1277, 11.1327,
            11.1347, 11.1432, 11.1467, 11.1497, 11.1552, 11.1582, 11.1607, 11.1677,
            11.1697, 11.1787, 11.1812, 11.1847, 11.1912, 11.1942, 11.2017, 11.2062]
Description:
  'Measurement of $R_b$ in $e^+e^-$ collisions by BaBar for energies between 10.541 and 11.206 GeV.
   The individual bottom hadronic and muonic cross sections are also outputted to the yoda file
   so that ratio $R$ can be recalculated if runs are combined.'
BibKey: Aubert:2008ab
BibTeX: '@article{Aubert:2008ab,
      author         = "Aubert, Bernard and others",
      title          = "{Measurement of the $e^{+} e^{-} \to b \bar{b}$ cross
                        section between $\sqrt{s}$ = 10.54-GeV and 11.20-GeV}",
      collaboration  = "BaBar",
      journal        = "Phys. Rev. Lett.",
      volume         = "102",
      year           = "2009",
      pages          = "012001",
      doi            = "10.1103/PhysRevLett.102.012001",
      eprint         = "0809.4120",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "SLAC-PUB-13397, BABAR-PUB-08042",
      SLACcitation   = "%%CITATION = ARXIV:0809.4120;%%"
}'
