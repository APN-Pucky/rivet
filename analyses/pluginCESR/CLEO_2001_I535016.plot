BEGIN PLOT /CLEO_2001_I535016/d01-x01-y01
Title=$\pi^+2\pi^-3\pi^0$ mass distribution in $\tau^-\to 2\pi^-\pi^+3\pi^0\nu_\tau$
XLabel=$m_{\pi^+2\pi^-3\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+2\pi^-3\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I535016/d01-x01-y02
Title=$2\pi^+3\pi^-\pi^0$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$
XLabel=$m_{2\pi^+3\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^+3\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
