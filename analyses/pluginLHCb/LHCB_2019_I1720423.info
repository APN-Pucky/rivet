Name: LHCB_2019_I1720423
Year: 2019
Summary: Dalitz plot analysis of $D^+\to K^+K^+K^-$
Experiment: LHCB
Collider: LHC
InspireID: 1720423
Status: VALIDATED NOHEPDATA
Reentrant:  true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 04 (2019) 063
RunInfo: Any process producing D+ -> K+ K+ K-
Description:
  'Measurement of the mass distributions in the decay $D^+\to K^+K^+K^-$.
   The data were read extracted from the plots in the paper and the backgrounds subtracted.
   Resolution/acceptance effects have been not unfolded and given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Herwig 7 events using model from paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2019tdw
BibTeX: '@article{LHCb:2019tdw,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Dalitz plot analysis of the $D^+\to K^-K^+K^+$ decay}",
    eprint = "1902.05884",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCb-PAPER-2018-039, CERN-EP-2018-336",
    doi = "10.1007/JHEP04(2019)063",
    journal = "JHEP",
    volume = "04",
    pages = "063",
    year = "2019"
}
'
