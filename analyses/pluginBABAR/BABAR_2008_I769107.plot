BEGIN PLOT /BABAR_2008_I769107/d01-x01-y01
Title=Photon energy spectrum for $\bar{B}\to X_s\gamma$
XLabel=$E_\gamma$ [GeV]
YLabel=$\frac{1}{\Gamma_B}\frac{\text{d}\Gamma}{\text{d}E_\gamma}$ [$10^{-4}\times\text{GeV}^{-1}$]
LogY=0
END PLOT
