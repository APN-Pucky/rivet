BEGIN PLOT /BESII_2006_I712991/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $J/\psi\to\gamma\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I712991/d02-x01-y01
Title=$\pi^0\pi^0$ mass distribution in $J/\psi\to\gamma\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
