Name: CMS_2019_I1697571
Year: 2019
Summary:  $B_s^0$ production at 5.02 TeV
Experiment: CMS
Collider: LHC
InspireID: 1697571
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 796 (2019) 168-190
 - CMS-HIN-17-008
 - arXiv:1810.03022 [hep-ex]
RunInfo: hadronic events
Beams: [p+, p+]
Energies: [5020]
Description:
  'Differential cross section in $p_\perp$ for $B^0_s$ production at 5.02 TeV'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CMS:2018eso
BibTeX: '@article{CMS:2018eso,
    author = "Sirunyan, Albert M and others",
    collaboration = "CMS",
    title = "{Measurement of B$^0_\mathrm{s}$ meson production in pp and PbPb collisions at $\sqrt{s_\mathrm{NN}} =$ 5.02 TeV}",
    eprint = "1810.03022",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CMS-HIN-17-008, CERN-EP-2018-257",
    doi = "10.1016/j.physletb.2019.07.014",
    journal = "Phys. Lett. B",
    volume = "796",
    pages = "168--190",
    year = "2019"
}
'
