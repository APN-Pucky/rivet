BEGIN PLOT /BESIII_2022_I2018236/d01-x01-y02
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to \gamma\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \gamma\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2018236/d01-x02-y02
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to \gamma\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \eta\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2018236/d02-x01-y02
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to \gamma\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \gamma\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2018236/d02-x02-y02
Title=$\eta^\prime\pi^+\pi^-$ mass distribution in $J/\psi\to \gamma\eta^\prime\pi^+\pi^-$ ($\eta^\prime\to \eta\pi^+\pi^-$)
XLabel=$m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^\prime\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
