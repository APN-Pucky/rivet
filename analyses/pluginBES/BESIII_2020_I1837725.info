Name: BESIII_2020_I1837725
Year: 2020
Summary: Measurement of the cross section for $e^+e^-\to2p2\bar{p}$ for $E_{\text{CMS}}$ between 4 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1837725
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2012.11079
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies: [4.009, 4.160, 4.180, 4.190, 4.200, 4.210, 4.220,
           4.230, 4.237, 4.246, 4.260, 4.270, 4.280, 4.290,
           4.315, 4.340, 4.360, 4.380, 4.400, 4.420, 4.440, 4.470, 4.600]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to2p2\bar{p}$ for $E_{\text{CMS}}$ between 4 and 4.6 GeV by the BESIII collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Ablikim:2020pjt
BibTeX: '@article{Ablikim:2020pjt,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Study of $e^+e^- \to 2(p\bar{p})$ at center-of-mass energies between 4.0 and 4.6 GeV}",
    eprint = "2012.11079",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "12",
    year = "2020"
}
'
