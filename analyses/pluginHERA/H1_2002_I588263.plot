BEGIN PLOT /H1_2002_I588263/d01-x01-y01
Title=Inclusive-jet $\sigma$ in $E_\mathrm{T}$, $-1.0 < \eta_{\text{lab}} < 0.5$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d01-x01-y02
Title=$0.5 < \eta_{\text{lab}} < 1.5$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d01-x01-y03
Title=$1.5 < \eta_{\text{lab}} < 2.8$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d02-x01-y01
Title=$5 < Q^2 < 10$ GeV$^2$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d02-x01-y02
Title=$10 < Q^2 < 20$ GeV$^2$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d02-x01-y03
Title=$20 < Q^2 < 35$ GeV$^2$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d02-x01-y04
Title=$35 < Q^2 < 70$ GeV$^2$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d02-x01-y05
Title=$70 < Q^2 < 100$ GeV$^2$
XLabel=$E_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{E_\mathrm{T}}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2002_I588263/d03-x01-y01
Title=$-1 < \eta_{\text{lab}} < 0.5$
XLabel=$E_\mathrm{T}^2 / Q^2$
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{(E_\mathrm{T}^2 / Q^2)}$ [pb]
END PLOT

BEGIN PLOT /H1_2002_I588263/d04-x01-y01
Title=$0.5 < \eta_{\text{lab}} < 1.5$
XLabel=$E_\mathrm{T}^2 / Q^2$
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{(E_\mathrm{T}^2 / Q^2)}$ [pb]
END PLOT

BEGIN PLOT /H1_2002_I588263/d05-x01-y01
Title=$1.5 < \eta_{\text{lab}} < 2.8$
XLabel=$E_\mathrm{T}^2 / Q^2$
YLabel=$\mathrm{d}{\sigma_\text{Jet}}/\mathrm{d}{(E_\mathrm{T}^2 / Q^2)}$ [pb]
END PLOT

