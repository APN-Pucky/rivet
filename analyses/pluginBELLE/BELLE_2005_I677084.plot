BEGIN PLOT /BELLE_2005_I677084/d01-x01-y01
Title=Helicty angle for $B^-\to D^{*-}D^0$
XLabel=$\cos\theta_{\mathrm{hel}}$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{\mathrm{hel}}$
LogY=0
END PLOT
