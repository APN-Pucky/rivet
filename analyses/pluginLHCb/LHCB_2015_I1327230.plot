BEGIN PLOT /LHCB_2015_I1327230/
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1327230/d0[1,2]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma(B_c^+)\text{Br}(B_c^+\to J/\psi\pi^+)/\mathrm{d}\sigma(B+)\text{Br}(B_c^+\to J/\psi K^+)$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2015_I1327230/d03
XLabel=$y$
YLabel=$\mathrm{d}\sigma(B_c^+)\text{Br}(B_c^+\to J/\psi\pi^+)/\mathrm{d}\sigma(B+)\text{Br}(B_c^+\to J/\psi K^+)$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2015_I1327230/d01-x01-y01
Title=Relative $B_c^+$ to $B^+$ production rate ($2.0<y<2.9$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1327230/d01-x01-y02
Title=Relative $B_c^+$ to $B^+$ production rate ($2.9<y<3.3$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1327230/d01-x01-y03
Title=Relative $B_c^+$ to $B^+$ production rate ($3.3<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1327230/d02-x01-y01
Title=Relative $B_c^+$ to $B^+$ production rate ($2.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1327230/d03-x01-y01
Title=Relative $B_c^+$ to $B^+$ production rate ($p_\perp<20$\,GeV) 
END PLOT
