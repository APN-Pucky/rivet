Name: BABAR_2014_I1287920
Year: 2014
Summary: Cross section for $e^+e^-\to$ $K_S^0K_L^0$, $K_S^0K_L^0\pi^+\pi^-$, $K_S^0K_S^0\pi^+\pi^-$, $K_S^0K_S^0K^+K^-$ between 1.06 and 2.2 GeV
Experiment: BABAR
Collider: PEP-II
InspireID: 1287920
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
- Phys.Rev. D89 (2014) 092002, 2014 
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [1.08, 1.12, 1.16, 1.2, 1.24, 1.28, 1.32, 1.36, 1.4, 1.425, 1.44, 1.475, 1.48, 1.52, 1.525, 1.56,
           1.575, 1.6, 1.625, 1.63, 1.64, 1.67, 1.675, 1.68, 1.72, 1.725, 1.73, 1.76, 1.77, 1.775, 1.8, 1.825,
           1.83, 1.84, 1.875, 1.88, 1.92, 1.925, 1.96, 1.975, 1.98, 2.0, 2.025, 2.03, 2.04, 2.05, 2.075, 2.08,
           2.12, 2.125, 2.13, 2.15, 2.16, 2.17, 2.175, 2.22, 2.225, 2.25, 2.275, 2.28, 2.325, 2.33, 2.35, 2.375,
           2.38, 2.42, 2.425, 2.45, 2.47, 2.475, 2.525, 2.53, 2.55, 2.575, 2.58, 2.625, 2.63, 2.65, 2.67, 2.675,
           2.72, 2.725, 2.75, 2.775, 2.78, 2.825, 2.83, 2.85, 2.875, 2.88, 2.92, 2.925, 2.95, 2.97, 2.975, 3.025,
           3.03, 3.05, 3.075, 3.08, 3.125, 3.13, 3.15, 3.17, 3.175, 3.22, 3.225, 3.25, 3.275, 3.28, 3.325, 3.33,
           3.35, 3.375, 3.38, 3.42, 3.425, 3.45, 3.47, 3.475, 3.525, 3.53, 3.55, 3.575, 3.58, 3.625, 3.63, 3.65,
           3.67, 3.675, 3.72, 3.725, 3.75, 3.775, 3.78, 3.825, 3.83, 3.85, 3.875, 3.88, 3.92, 3.925, 3.95, 3.97,
           3.975, 4.05, 4.15, 4.25, 4.35, 4.45]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for  $e^+e^-\to$ $K_S^0K_L^0$, $K_S^0K_L^0\pi^+\pi^-$, $K_S^0K_S^0\pi^+\pi^-$, $K_S^0K_S^0K^+K^-$ via radiative return,
   including for energies between 1.06 and 2.2 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords:
BibKey: Lees:2014xsh
BibTeX: '@article{Lees:2014xsh,
      author         = "Lees, J. P. and others",
      title          = "{Cross sections for the reactions $e^+ e^-\to K_S^0
                        K_L^0$, $K_S^0 K_L^0 \pi^+\pi^-$, $K_S^0 K_S^0
                        \pi^+\pi^-$, and $K_S^0 K_S^0 K^+K^-$ from events with
                        initial-state radiation}",
      collaboration  = "BaBar",
      journal        = "Phys. Rev.",
      volume         = "D89",
      year           = "2014",
      number         = "9",
      pages          = "092002",
      doi            = "10.1103/PhysRevD.89.092002",
      eprint         = "1403.7593",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "SLAC-PUB-15934",
      SLACcitation   = "%%CITATION = ARXIV:1403.7593;%%"
}'
