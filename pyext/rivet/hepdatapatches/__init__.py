from . import ALEPH_1996_I415745 # move average bins to separate objects
from . import ALICE_2014_I1244523 # pseudo-2D object should really be 1D
from . import ATLAS_2015_I1377585 # pseudo-3D object should really be 1D
from . import ARGUS_1997_I420421 # can't plot quantities in table, but can plot the sum
from . import ARGUS_1989_I262415 # split hist with overlapping bins
from . import ARGUS_1990_I295621 #  pseudo-2D object should really be 1D
from . import ARGUS_1997_I440304 # split hist with overlapping bins
from . import ATLAS_2018_I1711223 # use string edges
from . import ATLAS_2022_I2614196 # slice 2D object into 1D objects
from . import ATLAS_2023_I2648096 # slice long 1D object into sub 1D objects
from . import BABAR_2015_I1377201 # slice 2D object into 1D objects
from . import BABAR_2016_I1391152 # add seprate table for the "average" bin
from . import BELLE_2023_I2624324 #make 4 physical dists from merge 40 bin table
from . import BESIII_2018_I1641075 # subract background from observed correct with efficiency and normalise
from . import BESIII_2023_I2633025 # slice 2D object into sub 1D objects
from . import BESII_2004_I622224 # remove zero bins with no error
from . import CELLO_1983_I191415 # remove extra bin in overlap region
from . import CLEO_1985_I205668 # mask rogue bin
from . import CMS_2011_I878118 # pseudo-2D object should be 1D
from . import CMS_2015_I1342266 # pseudo-2D object should be 1D
from . import CMS_2017_I1485195 # slice 2D object into 1D objects
from . import CMS_2017_I1631985 # add a vector<1D> versions of the 2D AOs
#from . import DELPHI_1993_I356732 # edges apparently completely wrong
from . import DELPHI_1999_I499183 # swap x and y in table 39
from . import DELPHI_2000_I524693 # add missing point
from . import GAMMAGAMMA_1973_I84794 # reorder points
from . import H1_2002_I588263 # pseudo-3D objects should be 1D
from . import H1_2016_I1496981 # divide by Q2 bin width
from . import HRS_1987_I215848 # transform x-edges
from . import JADE_1990_I282847 # pseudo-2D object should really be 1D
from . import L3_1992_I334954 # mask "integral" bin
from . import LHCB_2013_I1244315 # 3 2D hist should be 3 1d hists
from . import MARKII_1985_I207785 # pseudo-2D object should really be 1D
from . import MARKII_1988_I246184 # mask last mega bin
from . import NUSEA_2003_I613362 # pseudo-3D object should really be 1D
from . import OPAL_1993_I342766 # pseudo-2D object should really be 1D
#from . import OPAL_1997_I440103 # lots of tables missing on HepData
from . import SLD_1999_I469925 # average bin
#from . import TASSO_1984_I194774 # d01 values missing on HepData
from . import TASSO_1986_I230950 # transform x-edges
from . import TASSO_1988_I263859 # remove sum bin and sort out multiplicity
from . import TASSO_1990_I294755 # remove sum bin
from . import TPC_1985_I205868 # pseudo-2D object should really be 1D
from . import ZEUS_1995_I392386 # pseudo-2D object should really be 1D
