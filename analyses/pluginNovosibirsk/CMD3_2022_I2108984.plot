BEGIN PLOT /CMD3_2022_I2108984/d01-x01-y01
Title=Cross Section for $e^+e^-\to K^0_S K^\pm\pi^\mp\pi^+\pi^-$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /CMD3_2022_I2108984/d02-x01-y01
Title=Cross Section for $e^+e^-\to f_1\pi^+\pi^-\to K^0_S K^\pm\pi^\mp\pi^+\pi^-$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT

BEGIN PLOT /CMD3_2022_I2108984/d02-x01-y02
Title=Cross Section for $e^+e^-\to f_1\pi^+\pi^-$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
