BEGIN PLOT /BESIII_2023_I2713388/d01-x01-y01
Title=$\Sigma^+\omega$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\omega$
XLabel=$m_{\Sigma^+\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2713388/d01-x01-y02
Title=$\bar\Sigma^-\omega$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\omega$
XLabel=$m_{\bar\Sigma^-\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Sigma^-\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2713388/d01-x01-y03
Title=$\Sigma^+\bar\Sigma^-$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\omega$
XLabel=$m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2713388/d02-x01-y01
Title=$\Sigma^+\phi$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\phi$
XLabel=$m_{\Sigma^+\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2713388/d02-x01-y02
Title=$\bar\Sigma^-\phi$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\phi$
XLabel=$m_{\bar\Sigma^-\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Sigma^-\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2713388/d02-x01-y03
Title=$\Sigma^+\bar\Sigma^-$ mass distribution in $\psi(2S)\to \Sigma^+\bar\Sigma^-\phi$
XLabel=$m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
