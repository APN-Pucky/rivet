BEGIN PLOT /BESIII_2023_I2670262/d01-x01-y01
Title=$\bar\Lambda^0\pi^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\Sigma^-$
XLabel=$m_{\bar\Lambda^0\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d01-x01-y02
Title=$\Sigma^-\pi^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\Sigma^-$
XLabel=$m_{\Sigma^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d01-x01-y03
Title=$\bar\Lambda^0\Sigma^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\Sigma^-$
XLabel=$m_{\bar\Lambda^0\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2670262/d02-x01-y01
Title=$\bar\Lambda^0\pi^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\bar\Sigma^+$
XLabel=$m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d02-x01-y02
Title=$\bar\Sigma^+\pi^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\bar\Sigma^+$
XLabel=$m_{\bar\Sigma^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Sigma^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d02-x01-y03
Title=$\bar\Lambda^0\bar\Sigma^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\bar\Sigma^+$
XLabel=$m_{\bar\Lambda^0\bar\Sigma^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\bar\Sigma^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2670262/d03-x01-y01
Title=$\bar\Lambda^0\pi^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\Sigma^+$
XLabel=$m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d03-x01-y02
Title=$\Sigma^+\pi^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\Sigma^+$
XLabel=$m_{\Sigma^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d03-x01-y03
Title=$\bar\Lambda^0\Sigma^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^-\Sigma^+$
XLabel=$m_{\bar\Lambda^0\Sigma^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\Sigma^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2670262/d04-x01-y01
Title=$\bar\Lambda^0\pi^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\bar\Sigma^-$
XLabel=$m_{\bar\Lambda^0\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d04-x01-y02
Title=$\bar\Sigma^-\pi^+$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\bar\Sigma^-$
XLabel=$m_{\bar\Sigma^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Sigma^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2670262/d04-x01-y03
Title=$\bar\Lambda^0\bar\Sigma^-$ mass distribution in $J/\psi\to \bar\Lambda^0\pi^+\bar\Sigma^-$
XLabel=$m_{\bar\Lambda^0\bar\Sigma^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\bar\Sigma^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
