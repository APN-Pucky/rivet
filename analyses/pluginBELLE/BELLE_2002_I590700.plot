BEGIN PLOT /BELLE_2002_I590700/d01-x01-y01
Title=$K^-K^{*0}$ mass in $B\to D^{(*)} K^-K^{*0}$
XLabel=$m_{K^-K^{*0}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2002_I590700/d02-x01-y01
Title=$K^-K^{*0}$ helicity angle in $B\to D K^-K^{*0}$
XLabel=$\cos\theta_{KK}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{KK}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I590700/d02-x01-y02
Title=$K^-K^{*0}$ helicity angle in $B\to D^* K^-K^{*0}$
XLabel=$\cos\theta_{KK}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{KK}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I590700/d02-x02-y01
Title=$K^*$ helicity angle in $B\to D K^-K^{*0}$
XLabel=$\cos\theta_{KK}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{KK}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I590700/d02-x02-y02
Title=$K^*$ helicity angle in $B\to D^* K^-K^{*0}$
XLabel=$\cos\theta_{KK}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{KK}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2002_I590700/d03-x01-y01
Title=$K^-K^0_S$ mass in $B^-\to D^0 K^-K^0_S$
XLabel=$m_{K^-K^{*0}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-K^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I590700/d03-x02-y01
Title=$K^-K^0_S$ helicity angle in $B^-\to D^0 K^-K^0_S$
XLabel=$\cos\theta_{KK}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{KK}$
LogY=0
END PLOT
