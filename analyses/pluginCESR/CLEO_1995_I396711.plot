BEGIN PLOT /CLEO_1995_I396711/d01-x01-y01
Title=Hadronic mass spectrum in $\tau$ decays
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
