BEGIN PLOT /BELLE_2004_I643565/d01-x01-y02
Title=$\sigma(e^+e^-\to D^{*+}D^{*-})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^{*+}D^{*-})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2004_I643565/d01-x01-y05
Title=$\sigma(e^+e^-\to D^{*+}D^{-}+\text{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to D^{*+}D^{-}+\text{c.c})$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BABAR_2003_I613283/d02-x01-y01
Title=$D^*$ helicity angle in $e^+e^-\to D^{*+}D^{-}+\text{c.c}$
XLabel=$\cos\theta$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta$
LogY=0
END PLOT
