BEGIN PLOT /SND_2022_I2102082/d01-x01-y01
Title=$\sigma(e^+e^-\to n\bar{n})$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to n\bar{n})$/nb
ConnectGaps=1
LogY=0
END PLOT
