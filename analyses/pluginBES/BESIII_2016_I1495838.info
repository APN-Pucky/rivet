Name: BESIII_2016_I1495838
Year: 2016
Summary: Cross section for $e^+e^-\to J/\psi\pi^+\pi^-$ at energies between 3.77 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1495838
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 118 (2017) no.9, 092001
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [3.773, 3.8077, 3.8874, 3.8924, 3.8962, 3.8974, 3.9024, 3.9074, 3.9124,
          3.9174, 3.9224, 3.9274, 3.9324, 3.9374, 3.9424, 3.9474, 3.9524, 3.9574,
          3.9624, 3.9674, 3.9724, 3.9774, 3.9824, 3.9874, 3.9924, 3.9974, 4.0024,
          4.0074, 4.0076, 4.0094, 4.0114, 4.0134, 4.0154, 4.0174, 4.0224, 4.0274,
          4.0324, 4.0374, 4.0474, 4.0524, 4.0574, 4.0624, 4.0674, 4.0774, 4.0855,
          4.0874, 4.0974, 4.1074, 4.1174, 4.1274, 4.1374, 4.1424, 4.1474, 4.1574,
          4.1674, 4.1774, 4.1874, 4.1886, 4.1924, 4.1974, 4.2004, 4.2034, 4.2074,
          4.2077, 4.2124, 4.2171, 4.2174, 4.2224, 4.2263, 4.2274, 4.2324, 4.2374,
          4.2404, 4.2417, 4.2424, 4.2454, 4.2474, 4.2524, 4.2574, 4.2580, 4.2624,
          4.2674, 4.2724, 4.2774, 4.2824, 4.2874, 4.2974, 4.3074, 4.3079, 4.3174,
          4.3274, 4.3374, 4.3474, 4.3574, 4.3583, 4.3674, 4.3774, 4.3874, 4.3924,
          4.3974, 4.4074, 4.4156, 4.4174, 4.4224, 4.4274, 4.4374, 4.4474, 4.4574,
          4.4671, 4.4774, 4.4974, 4.5174, 4.5271, 4.5374, 4.5474, 4.5574, 4.5674,
          4.5745, 4.5774, 4.5874, 4.5995]
Options:
 - ENERGY=*
Luminosity_fb: 
Description:
  'Measurement of the cross section for $e^+e^-\to J/\psi\pi^+\pi^-$ at energies between 3.77 and 4.6 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2016qzw
BibTeX: '@article{Ablikim:2016qzw,
      author         = "Ablikim, Medina and others",
      title          = "{Precise measurement of the $e^+e^-\to \pi^+\pi^-J/\psi$
                        cross section at center-of-mass energies from 3.77 to 4.60
                        GeV}",
      collaboration  = "BESIII",
      journal        = "Phys. Rev. Lett.",
      volume         = "118",
      year           = "2017",
      number         = "9",
      pages          = "092001",
      doi            = "10.1103/PhysRevLett.118.092001",
      eprint         = "1611.01317",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1611.01317;%%"
}'
