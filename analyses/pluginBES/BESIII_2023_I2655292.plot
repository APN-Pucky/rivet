BEGIN PLOT /BESIII_2023_I2655292
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y01
YLabel=$\alpha_{J\psi}$
Title=$\alpha_{J\psi}$ for $J/\psi\to\Sigma^+\bar{\Sigma}^-$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y02
YLabel=$\Delta\Phi$  [rad]
Title=$\Delta\Phi$ for $J/\psi\to\Sigma^+\bar{\Sigma}^-$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y03
YLabel=$\alpha_{\Sigma^+\to n \pi^+}$
Title=$\alpha$ for the decays $\Sigma^+\to n \pi^+$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y04
YLabel=$\alpha_{\bar\Sigma^-\to \bar{n} \pi^-}$
Title=$\alpha$ for the decays $\bar\Sigma^-\to \bar{n} \pi^-$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y05
YLabel=$\alpha_{\Sigma^+\to n \pi^+}/\alpha_{\Sigma^+\to p \pi^0}$
Title=Ratio of $\alpha$ for $\Sigma^+\to n \pi^+$ and $\Sigma^+\to p \pi^0$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y06
YLabel=$\alpha_{\bar\Sigma^+\to \bar{n} \pi^=}/\alpha_{\bar\Sigma^-\to \bar{p} \pi^0}$
Title=Ratio of $\alpha$ for $\bar\Sigma^-\to \bar{n} \pi^-$ and $\bar\Sigma^-\to \bar{p} \pi^0$
END PLOT
BEGIN PLOT /BESIII_2023_I2655292/d01-x01-y08
YLabel=$\alpha_{\Sigma^+\to n \pi^+}$
Title=$\alpha$ for the decays $\Sigma^+\to n \pi^+$ and  $\bar\Sigma^-\to \bar{n} \pi^-$ averaged
END PLOT
