BEGIN PLOT /CLEOIII_2005_I675005/d01-x01-y01
Title=Hadronic mass in $\tau^-\to K^-\pi^+\pi^-\pi^0\nu_\tau$ (ex. $\omega$)
XLabel=$m_{\mathrm{hadronic}}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\mathrm{hadronic}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2005_I675005/d01-x01-y02
Title=Hadronic mass in $\tau^-\to K^-\omega\nu_\tau$
XLabel=$m_{\mathrm{hadronic}}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\mathrm{hadronic}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2005_I675005/d01-x01-y03
Title=Hadronic mass in $\tau^-\to K^+K^-\pi^-\pi^0\nu_\tau$
XLabel=$m_{\mathrm{hadronic}}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\mathrm{hadronic}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
