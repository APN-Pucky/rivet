BEGIN PLOT /CLEO_1985_I218314/d04-x01-y01
Title=$\phi$ momentum spectrum
XLabel=$p$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
