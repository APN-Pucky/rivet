BEGIN PLOT /CLEO_1982_I180612/d01-x01-y01
Title=Kaon multplicity in the continumm
YLabel=$\langle N\rangle$
LogY=0
XCustomMajorTicks=1  $K^+$    2 $K^0$
END PLOT
BEGIN PLOT /CLEO_1982_I180612/d01-x01-y02
Title=Kaon multplicity in $\Upsilon(4S)$ decays
YLabel=$\langle N\rangle$
LogY=0
XCustomMajorTicks=1  $K^+$    2 $K^0$
END PLOT

BEGIN PLOT /CLEO_1982_I180612/d02-x01-y01
Title=$K^+$ momentum, continuum
XLabel=$|p|$ [GeV]
YLabel=$\text{d}\sigma/\text{d}|p|$ [$\text{nb}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1982_I180612/d02-x02-y01
Title=$K^0$ momentum, continuum
XLabel=$|p|$ [GeV]
YLabel=$\text{d}\sigma/\text{d}|p|$ [$\text{nb}/\text{GeV}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_1982_I180612/d02-x01-y02
Title=$K^+$ momentum, $\Upsilon(4S)$
XLabel=$|p|$ [GeV]
YLabel=$\text{d}\sigma/\text{d}|p|$ [$\text{nb}/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1982_I180612/d02-x02-y02
Title=$K^0$ momentum, $\Upsilon(4S)$
XLabel=$|p|$ [GeV]
YLabel=$\text{d}\sigma/\text{d}|p|$ [$\text{nb}/\text{GeV}$]
LogY=0
END PLOT

