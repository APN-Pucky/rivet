BEGIN PLOT /BABAR_2013_I1217425/d01-x01-y01
Title=$\Sigma_c^0\pi^+$ mass  distribution in $\bar{B}^0\to\Sigma_c^{0}\bar{p}\pi^+$
XLabel=$m_{\Sigma_c^0\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^0\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d01-x01-y02
Title=$\Sigma_c^0\bar{p}$ mass  distribution in $\bar{B}^0\to\Sigma_c^{0}\bar{p}\pi^+$
XLabel=$m_{\Sigma_c^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^0\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d01-x01-y03
Title=$\bar{p}\pi^+$ mass  distribution in $\bar{B}^0\to\Sigma_c^{0}\bar{p}\pi^+$
XLabel=$m_{\bar{p}\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2013_I1217425/d02-x01-y01
Title=$\Sigma_c^{++}\pi^-$ mass  distribution in $\bar{B}^0\to\Sigma_c^{++}\bar{p}\pi^-$
XLabel=$m_{\Sigma_c^{++}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{++}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d02-x01-y02
Title=$\Sigma_c^{++}\bar{p}$ mass  distribution in $\bar{B}^0\to\Sigma_c^{++}\bar{p}\pi^-$
XLabel=$m_{\Sigma_c^{++}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{++}\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d02-x01-y03
Title=$\bar{p}\pi^-$ mass  distribution in $\bar{B}^0\to\Sigma_c^{++}\bar{p}\pi^-$
XLabel=$m_{\bar{p}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2013_I1217425/d03-x01-y01
Title=$\Sigma_c^{*++}\pi^-$ mass  distribution in $\bar{B}^0\to\Sigma_c^{*++}\bar{p}\pi^-$
XLabel=$m_{\Sigma_c^{*++}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{*++}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d03-x01-y02
Title=$\Sigma_c^{*++}\bar{p}$ mass  distribution in $\bar{B}^0\to\Sigma_c^{*++}\bar{p}\pi^-$
XLabel=$m_{\Sigma_c^{*++}\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma_c^{*++}\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d03-x01-y03
Title=$\bar{p}\pi^-$ mass  distribution in $\bar{B}^0\to\Sigma_c^{*++}\bar{p}\pi^-$
XLabel=$m_{\bar{p}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y01
Title=$\Lambda_c^+\pi^+$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y02
Title=$\Lambda_c^+\pi^-$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y03
Title=$\bar{p}\pi^+$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\bar{p}\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y04
Title=$\bar{p}\pi^-$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\bar{p}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y05
Title=$\pi^+\pi^-$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d04-x01-y06
Title=$\Lambda_c^+\bar{p}$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2013_I1217425/d05-x01-y01
Title=$\Lambda_c^+\pi^+\pi^-$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d05-x01-y02
Title=$\bar{p}\pi^+\pi^-$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\bar{p}\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d05-x01-y03
Title=$\Lambda_c^+\pi^-\bar{p}$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\pi^-\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\pi^-\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2013_I1217425/d05-x01-y04
Title=$\Lambda_c^+\pi^+\bar{p}$ mass  distribution in $\bar{B}^0\to\Lambda_c^+\bar{p}\pi^+\pi^-$
XLabel=$m_{\Lambda_c^+\pi^+\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\pi^+\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
