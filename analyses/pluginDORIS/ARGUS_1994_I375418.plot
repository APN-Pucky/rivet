BEGIN PLOT /ARGUS_1994_I375418/
XLabel=$\sqrt{s}$
YLabel=$\sigma$ [nb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /ARGUS_1994_I375418/d01-x01-y01
Title=Visible cross section fo $e^+e^-\to\text{hadrons}$ near the $\Upsilon(1S)$
END PLOT
BEGIN PLOT /ARGUS_1994_I375418/d02-x01-y01
Title=Visible cross section fo $e^+e^-\to\text{hadrons}$ near the $\Upsilon(2S)$
END PLOT
BEGIN PLOT /ARGUS_1994_I375418/d03-x01-y01
Title=Visible cross section fo $e^+e^-\to\text{hadrons}$ near the $\Upsilon(4S)$
END PLOT
