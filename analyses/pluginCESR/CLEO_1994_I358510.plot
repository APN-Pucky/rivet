BEGIN PLOT /CLEO_1994_I358510/d01-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to p\bar{p})$ [nb]
LogY=1
END PLOT
BEGIN PLOT /CLEO_1994_I358510/d02-x01-y01
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.0<\sqrt{s}<2.5$ GeV
END PLOT
BEGIN PLOT /CLEO_1994_I358510/d03-x01-y01
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.5<\sqrt{s}<3.0$ GeV
END PLOT
