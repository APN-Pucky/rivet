BEGIN PLOT /BABAR_2005_I686573/d01-x01-y01
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^+$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p\bar{p}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I686573/d02-x01-y01
Title=$pK^+$ mass for $B^+\to p\bar{p}K^+$ ($m_{pK^+}>m_{\bar{p}K^+}$)
XLabel=$m_{pK^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{pK^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I686573/d02-x01-y02
Title=$\bar{p}K^+$ mass for $B^+\to p\bar{p}K^+$ ($m_{pK^+}<m_{\bar{p}K^+}$)
XLabel=$m_{\bar{p}K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I686573/d02-x01-y03
Title=Difference between $pK^+$ and $\bar{p}K^+$ distributions for $B^+\to p\bar{p}K^+$
XLabel=$m_{p/\bar{p}K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p/\bar{p}K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
