Name: BESIII_2017_I1624548
Year: 2017
Summary: Analysis of $\psi(2S)\to\gamma\chi_{c2}$ decays using $\chi_{c2}\to\gamma\gamma$
Experiment: BESIII
Collider: BEPC
InspireID: 1624548
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 96 (2017) 9, 092007
RunInfo: e+e- > psi(2S) 
Beams: [e-, e+]
Energies: [3.686]
Description:
  'Analysis of the angular distribution of the photons and leptons produced in
   $e^+e^-\to \psi(2S) \to \gamma\chi_{c2}$ followed by $\chi_{c2}\to\gamma \gamma$.
   Gives information about the decay and is useful for testing correlations in charmonium decays.
   N.B. the data was read from the figures in the paper and is not corrected and should only be used qualatively.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2017rpg
BibTeX: '@article{BESIII:2017rpg,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Improved measurements of two-photon widths of the $\chi_{cJ}$ states and helicity analysis for $\chi_{c2}\to\gamma\gamma$}",
    eprint = "1709.06742",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.96.092007",
    journal = "Phys. Rev. D",
    volume = "96",
    number = "9",
    pages = "092007",
    year = "2017"
}
'
