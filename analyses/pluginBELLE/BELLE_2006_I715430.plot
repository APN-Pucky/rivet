BEGIN PLOT /BELLE_2006_I715430/d01-x01-y01
Title=$f_+(q^2)$ for $D^0\to K^-\ell^+\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$f_+(q^2)$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I715430/d02-x01-y01
Title=$f_+(q^2)$ for $D^0\to \pi^-\ell^+\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$f_+(q^2)$
LogY=0
END PLOT
