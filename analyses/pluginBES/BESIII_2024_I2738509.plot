BEGIN PLOT /BESIII_2024_I2738509/d01-x01-y02
Title=Cross section for $e^+e^-\to K^0_SK^0_L$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
END PLOT
