BEGIN PLOT /ATLAS_2024_I2762099/d22-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 20$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(\ell_WZ)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2762099/d23-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 40$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(\ell_WZ)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2762099/d24-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 70$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(\ell_WZ)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2762099/d25-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 20$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(WZ)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2762099/d26-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 40$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(WZ)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2762099/d27-x01-y01
Title=TT state in the RAZ Region $p_\text{T}^{WZ}< 70$ GeV
XLabel=$|\Delta Y(l_{W}Z)|$
YLabel=$\frac{1}{\sigma} \frac{\Delta\sigma}{\Delta |\Delta Y(WZ)|}$
END PLOT

