BEGIN PLOT /BABAR_2009_I813140
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I813140/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $B^\pm\to\pi^\pm\pi^\pm\pi^\mp$
END PLOT

BEGIN PLOT /BABAR_2009_I813140/d02-x01-y01
Title=$\pi^+\pi^-$ mass in $B^+\to\pi^+\pi^+\pi^-$
END PLOT
BEGIN PLOT /BABAR_2009_I813140/d02-x01-y02
Title=$\pi^+\pi^-$ mass in $B^-\to\pi^-\pi^-\pi^+$
END PLOT
BEGIN PLOT /BABAR_2009_I813140/d02-x02-y01
Title=$\pi^+\pi^-$ mass in $B^+\to\pi^+\pi^+\pi^-$ ($\cos\theta_H>0$)
END PLOT
BEGIN PLOT /BABAR_2009_I813140/d02-x02-y02
Title=$\pi^+\pi^-$ mass in $B^-\to\pi^-\pi^-\pi^+$ ($\cos\theta_H>0$)
END PLOT
BEGIN PLOT /BABAR_2009_I813140/d02-x03-y01
Title=$\pi^+\pi^-$ mass in $B^+\to\pi^+\pi^+\pi^-$ ($\cos\theta_H<0$)
END PLOT
BEGIN PLOT /BABAR_2009_I813140/d02-x03-y02
Title=$\pi^+\pi^-$ mass in $B^-\to\pi^-\pi^-\pi^+$ ($\cos\theta_H<0$)
END PLOT
