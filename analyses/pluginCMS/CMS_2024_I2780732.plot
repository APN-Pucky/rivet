BEGIN PLOT /CMS_2024_I2780732/d02-x01-y01
Title=CMS, 13 TeV, multijet ratio
XLabel=$p_\mathrm{T}$ [GeV]
YLabel=$R_{\Delta\phi} (p_\mathrm{T})$
LogY=0
LogX=1
YMin=0.10
YMax=0.45
END PLOT
