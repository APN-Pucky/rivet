BEGIN PLOT /BESIII_2022_I2033007
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /BESIII_2022_I2033007/d01-x01-y01
Title=Cross section for $e^+e^-\to K^+K^-\pi^0$
END PLOT
BEGIN PLOT /BESIII_2022_I2033007/d02-x01-y01
Title=Cross section for $e^+e^-\to \phi\pi^0$
END PLOT
BEGIN PLOT /BESIII_2022_I2033007/d03-x01-y01
Title=Cross section for $e^+e^-\to K^*_2(1430))^+K^-$ +c.c.
END PLOT
BEGIN PLOT /BESIII_2022_I2033007/d04-x01-y01
Title=Cross section for $e^+e^-\to K^{*+}K^-$ +c.c.
END PLOT
