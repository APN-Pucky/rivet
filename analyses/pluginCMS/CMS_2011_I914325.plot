BEGIN PLOT /CMS_2011_I914325/d01-x01-y01
Title=$B^0_s$ cross section ($8<p_\perp<50$\,GeV, $|y|<2.4$)
XLabel=
YLabel=$\sigma$ [nb]
END PLOT
BEGIN PLOT /CMS_2011_I914325/d02-x01-y01
Title=$B^0_s$ differential cross section ($|y|<2.4$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2011_I914325/d03-x01-y01
Title=$B^0_s$ differential cross section ($8<p_\perp<50$\,GeV)
XLabel=$y$
YLabel=$\text{d}\sigma/\text{d}y$ [nb]
END PLOT

