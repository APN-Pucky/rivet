BEGIN PLOT /H1_2016_I1496981/d01-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$5.5<Q^2<8$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d02-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$8<Q^2<11$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d03-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$11<Q^2<16$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d04-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$16<Q^2<22$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d05-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$22<Q^2<30$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d06-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$30<Q^2<42$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d07-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$42<Q^2<60$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d08-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$60<Q^2<80$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d51-x01-y01
Title=Inclusive jet cross section in $Q^2$
XLabel=$Q^2$ [GeV$^2$]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$ 5 < P_\mathrm{T} < 7 $ GeV
LogX=1
END PLOT

BEGIN PLOT /H1_2016_I1496981/d09-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$5.5<Q^2<8$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d10-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$8<Q^2<11$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d11-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$11<Q^2<16$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d12-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$16<Q^2<22$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d13-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$22<Q^2<30$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d14-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$30<Q^2<42$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d15-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$42<Q^2<60$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d16-x01-y01
Title=Dijet production cross section in $\langle P_\mathrm{T} \rangle_2$
XLabel=$\langle P_\mathrm{T} \rangle_2$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_2)$ [pb/GeV$^3$]
CustomLegend=$60<Q^2<80$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d17-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$5.5<Q^2<8$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d18-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$8<Q^2<11$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d19-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$11<Q^2<16$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d20-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$16<Q^2<22$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d21-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$22<Q^2<30$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d22-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$30<Q^2<42$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d23-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$42<Q^2<60$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d24-x01-y01
Title=Trijet production cross section in $\langle P_\mathrm{T} \rangle_3$
XLabel=$\langle P_\mathrm{T} \rangle_3$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta \langle P_\mathrm{T} \rangle_3)$ [pb/GeV$^3$]
CustomLegend=$60<Q^2<80$ GeV$^2$
END PLOT

BEGIN PLOT /H1_2016_I1496981/d01-x01-y01
Title=Inclusive jet production cross section in $P_\mathrm{T}^\mathrm{jet}$
XLabel=$P_\mathrm{T}^\mathrm{jet}$ [GeV]
YLabel=$\sigma / (\Delta Q^2 \Delta P_\mathrm{T})$ [pb/GeV$^3$]
CustomLegend=$5.5<Q^2<8$ GeV$^2$
END PLOT
