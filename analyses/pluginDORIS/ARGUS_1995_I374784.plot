BEGIN PLOT /ARGUS_1995_I374784/d01-x01-y01
Title=$e^-$ momentum spectrum in $\tau$ pseudo-rest frame
XLabel=$|p|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ $\text{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1995_I374784/d01-x01-y02
Title=$\mu^-$ momentum spectrum in $\tau$ pseudo-rest frame
XLabel=$|p|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ $\text{GeV}^{-1}$
LogY=0
END PLOT
