BEGIN PLOT /LHCB_2012_I1087907/d01-x01-y01
Title=Ratio of Prompt $\chi_{c2}$ to $\chi_{c1}$ as function of $p_\perp$ J/$\psi$ in $J/\psi\gamma$ decay 
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$\sigma(\chi_{c2})/\sigma(\chi_{c1})$
LogY=0
END PLOT
