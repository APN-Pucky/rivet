Name: CMD2_2004_I648023
Year: 2004
Summary: Cross section for $e^+e^-\to\pi^+\pi^+\pi^-\pi^+$ between 0.98 and 1.38 GeV
Experiment: CMD2
Collider: VEPP-2M
InspireID: 648023
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett. B595 (2004) 101-108, 2004 
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [0.980, 1.040, 1.050, 1.060, 1.070, 1.080, 1.090, 1.100, 1.110, 1.120, 1.130,
           1.140, 1.150, 1.160, 1.170, 1.180, 1.190, 1.200, 1.210, 1.220, 1.230, 1.240,
           1.250, 1.260, 1.270, 1.280, 1.290, 1.300, 1.310, 1.320, 1.330, 1.340, 1.350, 1.360, 1.370, 1.380] 
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\pi^+\pi^+\pi^-\pi^+$ between 0.98 and 1.38 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords:
BibKey: Akhmetshin:2004dy
BibTeX: '@article{Akhmetshin:2004dy,
      author         = "Akhmetshin, R. R. and others",
      title          = "{Total cross section of the process e+ e- ---&gt; pi+ pi-
                        pi+ pi- in the C.M. energy range 980-MeV to 1380-MeV}",
      collaboration  = "CMD-2",
      journal        = "Phys. Lett.",
      volume         = "B595",
      year           = "2004",
      pages          = "101-108",
      doi            = "10.1016/j.physletb.2004.05.056",
      eprint         = "hep-ex/0404019",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = HEP-EX/0404019;%%"
}'
