BEGIN PLOT /BELLE_2005_I679165/d01-x01-y01
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^+$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I679165/d01-x01-y02
Title=$p\bar{p}$ mass for $B^0\to p\bar{p}K^0_S$
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I679165/d01-x01-y03
Title=$p\bar{\Lambda}$ mass for $B^0\to p\bar{\Lambda}\pi^-$
XLabel=$m_{p\bar{\Lambda}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I679165/d02-x01
XLabel=$\cos\theta_p$
YLabel=$\text{d}\mathcal{B}/\text{d}\cos\theta_p\times10^6$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I679165/d02-x01-y01
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($m_{p\bar{p}}<2.85\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2005_I679165/d02-x01-y02
Title=$\cos\theta_p$ for $B^0\to p\bar{p}K^0_S$ ($m_{p\bar{p}}<2.85\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2005_I679165/d02-x01-y03
Title=$\cos\theta_p$ for $B^0\to p\bar{\Lambda}\pi^-$ ($m_{p\bar{p}}<2.85\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2005_I679165/d02-x01-y04
Title=$\cos\theta_p$ for $B^0\to p\bar{\Lambda}\pi^-$ ($p\pi^-$ system)
END PLOT
