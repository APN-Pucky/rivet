# BEGIN PLOT /D0_2007_I744624/d01-x01-y01
Title=Inclusive Z boson rapidity
XLabel=$|y|$(Z)
YLabel=$1/\sigma \; \mathrm{d}\sigma/\mathrm{d}|y|(Z)$
# END PLOT

