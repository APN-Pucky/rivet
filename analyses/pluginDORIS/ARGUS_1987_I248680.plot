BEGIN PLOT /ARGUS_1987_I248680
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d02-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d03-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}K^-\pi^+$ + c.c. (non-resonant)
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$ (non-resonant)
END PLOT

BEGIN PLOT /ARGUS_1987_I248680/d05-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d05-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$ (non-resonant)
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d05-x01-y03
Title=Cross section for $\gamma\gamma\to K^{*0}K^-\pi^+$ + c.c. (non-resonant)
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d05-x01-y04
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$
END PLOT
BEGIN PLOT /ARGUS_1987_I248680/d05-x01-y05
Title=Cross section for $\gamma\gamma\to \phi\pi^+\pi^-$
END PLOT
