BEGIN PLOT /BELLE_2017_I1499706/d01-x01-y01
Title=Spectrum of $\chi_{c1}$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}x_p\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1499706/d01-x02-y01
Title=Branching ratio for $\chi_{c1}+X$ in $\Upsilon(1S)$ decays
XLabel=$x_p$
YLabel=$\mathcal{B}\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1499706/d01-x01-y02
Title=Spectrum of $\chi_{c1}$ in $\Upsilon(2S)$ decays
XLabel=$x_p$
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}x_p\times10^4$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1499706/d01-x02-y02
Title=Branching ratio for $\chi_{c1}+X$ in $\Upsilon(2S)$ decays
XLabel=$x_p$
YLabel=$\mathcal{B}\times10^4$
LogY=0
END PLOT
