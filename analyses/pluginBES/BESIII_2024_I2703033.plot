BEGIN PLOT /BESIII_2024_I2703033
LogY=0
END PLOT

BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y01
Title=$\alpha_\psi$ for $J/\psi\to \Xi^-\bar\Xi^+$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y02
Title=$\Delta\Phi$ for $J/\psi\to \Xi^-\bar\Xi^+$
YLabel=$\Delta\Phi$
END PLOT

BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y03
Title=$\alpha_\Xi$ for $\Xi\to\Lambda\pi^-$
YLabel=$\alpha_\Xi$
YMax=0
YMin=-0.8
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y05
Title=$\bar{\alpha}_\Xi$ for $\bar\Xi\to\bar\Lambda\pi^+$
YLabel=$\bar{\alpha}_\Xi$
END PLOT

BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y07
Title=$\alpha_\Lambda$ for $\Lambda\to p\pi^-$
YLabel=$\alpha_\Lambda$
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y08
Title=$\bar{\alpha}_\Lambda$ for $\bar\Lambda\to\bar{p}\pi^+$
YLabel=$\bar{\alpha}_\Lambda$
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y09
Title=$\alpha_\Lambda$ for $\Lambda\to n\pi^0$
YLabel=$\alpha_\Lambda$
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y10
Title=$\bar{\alpha}_\Lambda$ for $\bar\Lambda\to\bar{n}\pi^0$
YLabel=$\bar{\alpha}_\Lambda$
END PLOT


BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y18
Title=Ratio of $\alpha(\Lambda\to n\pi^0)/\alpha(\Lambda\to p\pi^-)$
YLabel=$\alpha_{\Lambda_0}/\alpha_{\Lambda_-}$
END PLOT
BEGIN PLOT /BESIII_2024_I2703033/d01-x01-y19
Title=Ratio of $\alpha(\bar\Lambda\to\bar{n}\pi^0)/\alpha(\bar\Lambda\to\bar{p}\pi^+)$
YLabel=$\tilde{\alpha}_{\Lambda_0}/\alpha_{\Lambda_+}$
END PLOT
