# BEGIN PLOT /LHCB_2021_I1990313/d18-x01-y01
Title=Single differential cross-section in $y^Z$ bins
XLabel=$y^Z$
YLabel=$\frac{d\sigma}{dy^{Z}}$ [pb]
LeftMargin=1.8
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d19-x01-y01
Title=Single differential cross-section in $p_{T}^{Z}$ bins
XLabel=$p_{T}^{Z}$ [GeV/$c$]
YLabel=$\frac{d\sigma}{dp_{T}^{Z}}$ [pb/(GeV/$c$)]
LeftMargin=1.8
LogY=1
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d20-x01-y01
Title=Single differential cross-section in $\phi_{\eta}^{*}$ bins
XLabel=$\phi_{\eta}^{*}$
YLabel=$\frac{d\sigma}{d\phi_{\eta}^{*}}$ [pb]
LeftMargin=1.8
LogY=1
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d21-x01-y0.
XLabel=$p_{T}^{Z}$ [GeV/$c$]
YLabel=$\frac{d\sigma^{2}}{dy^{Z}dp_{T}^{Z}}$ [pb/(GeV/$c$)]
LeftMargin=1.8
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d22-x01-y0.
XLabel=$\phi_{\eta}^{*}$
YLabel=$\frac{d\sigma^{2}}{dy^{Z}d\phi_{\eta}^{*}}$ [pb]
LeftMargin=1.8
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d2[1-2]-x01-y01
Title=Double differential cross-section for $2.0 < y^{Z} < 2.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d2[1-2]-x01-y02
Title=Double differential cross-section for $2.5 < y^{Z} < 3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d2[1-2]-x01-y03
Title=Double differential cross-section for $3.0 < y^{Z} < 3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d2[1-2]-x01-y04
Title=Double differential cross-section for $3.5 < y^{Z} < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1990313/d2[1-2]-x01-y05
Title=Double differential cross-section for $4.0 < y^{Z} < 4.5$
# END PLOT

