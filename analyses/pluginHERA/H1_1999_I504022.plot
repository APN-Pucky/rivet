BEGIN PLOT /H1_1999_I504022/d01-x01-y01
Title= $\pi^0$ meson production cross-sections $p_\mathrm{T}^*>2.5$
XLabel= x
YLabel= d$\sigma$/dx [nb]
LogX=1
END PLOT

BEGIN PLOT /H1_1999_I504022/d02-x01-y01
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[2.0-4.5]
XLabel=$\eta$
YLabel=d$\sigma$/d$\eta$ [pb]
END PLOT

BEGIN PLOT /H1_1999_I504022/d03-x01-y01
LogX=0
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[2.0-4.5]
XLabel=$p_\mathrm{T}^*$ [GeV]
YLabel=d$\sigma$/$dp_\mathrm{T}^*$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_1999_I504022/d04-x01-y01
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[4.5-15.0]
XLabel= x
YLabel= d$\sigma$/dx [nb]
LogX=1
END PLOT

BEGIN PLOT /H1_1999_I504022/d05-x01-y01
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[4.5-15.0]
XLabel= $\eta$
YLabel= d$\sigma$/d$\eta$ [pb]
END PLOT

BEGIN PLOT /H1_1999_I504022/d06-x01-y01
LogX=0
Title= $\pi ^0$ meson production cross-sections $Q^2$/GeV=[4.5-15.0]
XLabel= $p_\mathrm{T}^*$ [GeV]
YLabel= d$\sigma$/$dp_\mathrm{T}^*$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_1999_I504022/d07-x01-y01
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[15.0-70.0]
XLabel= x
YLabel= d$\sigma$/dx [nb]
LogX=1
END PLOT

BEGIN PLOT /H1_1999_I504022/d08-x01-y01
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[15.0-70.0]
XLabel=$\eta$
YLabel=d$\sigma$/$d\eta$ [pb]
END PLOT

BEGIN PLOT /H1_1999_I504022/d09-x01-y01
#LogX=1
Title=$\pi^0$ meson production cross-sections $Q^2$/GeV=[15.0-70.0]
XLabel=$p_\mathrm{T}^*$ [GeV]
YLabel=d$\sigma$/$dp_\mathrm{T}^*$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_1999_I504022/d10-x01-y01
Title=$\pi^0$ meson production cross-sections $p_\mathrm{T}^*>2.5$ GeV
XLabel=$Q^2$ [GeV$^2$]
YLabel=d$\sigma$/d$Q^2$ [pb/GeV$^2$]
LogX=1
END PLOT

BEGIN PLOT /H1_1999_I504022/d11-x01-y01
Title= $\pi^0$ meson production cross-sections $p_\mathrm{T}^*>3.5$ GeV
XLabel= x
YLabel= d$\sigma$/dx [nb]
LogX=1
END PLOT

BEGIN PLOT /H1_1999_I504022/d12-x01-y01
Title=$\pi^0$ meson production cross-sections $p_\mathrm{T}^*>3.5$ GeV
XLabel=$Q^2$ [GeV$^2$]
YLabel=d$\sigma$/d$Q^2$ [pb/GeV$^2$]
LogX=1
END PLOT
