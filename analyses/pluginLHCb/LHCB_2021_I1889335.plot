# BEGIN PLOT /LHCB_2021_I1889335/.*
XLabel=$p_\mathrm{T}$ [GeV/c]
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_\mathrm{T} \mathrm{d}\eta$ [$m\mathrm{b}/(\mathrm{GeV}\!/\!c)$]
LogX=1
LogY=1
# END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y01
Title=Negative charge particle multiplicity $2.0 \le \eta < 2.5$
XMin=0.5
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y02
Title=Negative charge particle multiplicity $2.5 \le \eta < 3.0$
XMin=0.3
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y03
Title=Negative charge particle multiplicity $3.0 \le \eta < 3.5$
XMin=0.2
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y04
Title=Negative charge particle multiplicity $3.5 \le \eta < 4.0$
XMin=0.1
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y05
Title=Negative charge particle multiplicity $4.0 \le \eta < 4.5$
XMin=0.07
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d01-x01-y06
Title=Negative charge particle multiplicity $4.5 \le \eta < 4.8$
XMin=0.07
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y01
Title=Positive charge particle multiplicity $2.0 \le \eta < 2.5$
XMin=0.5
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y02
Title=Positive charge particle multiplicity $2.5 \le \eta < 3.0$
XMin=0.3
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y03
Title=Positive charge particle multiplicity $3.0 \le \eta < 3.5$
XMin=0.2
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y04
Title=Positive charge particle multiplicity $3.5 \le \eta < 4.0$
XMin=0.1
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y05
Title=Positive charge particle multiplicity $4.0 \le \eta < 4.5$
XMin=0.07
END PLOT

BEGIN PLOT /LHCB_2021_I1889335/d02-x01-y06
Title=Positive charge particle multiplicity $4.5 \le \eta < 4.8$
XMin=0.07
END PLOT
