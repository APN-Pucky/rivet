BEGIN PLOT /BESIII_2023_I2705058/d01-x01-y01
Title=$\sigma(e^+e^-\to \phi\eta^\prime)$ (Dressed)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi\eta^\prime)$ [pb]
ConnectGaps=1
#LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2705058/d01-x01-y02
Title=$\sigma(e^+e^-\to \phi\eta^\prime)$ (Born)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi\eta^\prime)$ [pb]
ConnectGaps=1
#LogY=0
END PLOT
