# BEGIN PLOT /ALEPH_1996_I398193/d01-x01-y01
Title=Photon Fragmentation in 2-jet events with $y_\mathrm{cut}=0.01$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{2-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d02-x01-y01
Title=Photon Fragmentation in 2-jet events with $y_\mathrm{cut}=0.06$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{2-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d03-x01-y01
Title=Photon Fragmentation in 2-jet events with $y_\mathrm{cut}=0.1$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{2-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d04-x01-y01
Title=Photon Fragmentation in 2-jet events with $y_\mathrm{cut}=0.33$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{2-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d05-x01-y01
Title=Photon Fragmentation in 3-jet events with $y_\mathrm{cut}=0.01$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{3-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d06-x01-y01
Title=Photon Fragmentation in 3-jet events with $y_\mathrm{cut}=0.06$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{3-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d07-x01-y01
Title=Photon Fragmentation in 3-jet events with $y_\mathrm{cut}=0.1$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{3-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
  
# BEGIN PLOT /ALEPH_1996_I398193/d08-x01-y01
Title=Photon Fragmentation in 4-jet events with $y_\mathrm{cut}=0.01$
XLabel=$z_\gamma$
YLabel=$1/\sigma \mathrm{d}\sigma(\mathrm{4-jet})/\mathrm{d}z \times 10^3$
LogY=1
RatioPlotMode=deviation
# END PLOT
