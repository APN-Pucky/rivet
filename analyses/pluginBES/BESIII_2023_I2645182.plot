BEGIN PLOT /BESIII_2023_I2645182/d01-x01-y01
Title=$D_s^+\to f_0(\to\pi^+\pi^-) e^+ \nu_e$
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
