BEGIN PLOT /E791_2002_I585322/d01-x01-y01
Title=Lower $K^-\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /E791_2002_I585322/d01-x01-y02
Title=Higher $K^-\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /E791_2002_I585322/dalitz
Title=Dalitz plot for $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{K^-\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
