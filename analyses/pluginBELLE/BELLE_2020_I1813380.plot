BEGIN PLOT /BELLE_2020_I1813380/d01-x01-y01
Title=$\eta\Lambda$ mass distribution in $\Lambda_c^+\to \eta\Lambda\pi^+$
XLabel=$m_{\eta\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2020_I1813380/d01-x01-y02
Title=$\Lambda\pi^+$ mass distribution in $\Lambda_c^+\to \eta\Lambda\pi^+$
XLabel=$m_{\Lambda\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
