BEGIN PLOT /BELLE_2012_I1090664/d01-x0[1,2,3]-y01
LogY=0
END PLOT
BEGIN PLOT /BELLE_2012_I1090664/d01-x01-y0
Title=Cross section for $\gamma\gamma\to \phi\omega$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2012_I1090664/d01-x02-y0
Title=Cross section for $\gamma\gamma\to \phi\phi$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2012_I1090664/d01-x03-y0
Title=Cross section for $\gamma\gamma\to \omega\omega$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT
