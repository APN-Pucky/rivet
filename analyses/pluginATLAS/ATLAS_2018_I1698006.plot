# BEGIN PLOT /ATLAS_2018_I1698006/* 
LogY=1
XTwosidedTicks=1
YTwosidedTicks=1
LeftMargin=1.5
#Title=
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d01-x01-y01
#Title= Measured integrated cross sections for the $Z\mathrm{\gamma}$ process for neutrino final states at\n $\sqrt{s}=13$ TeV in the extended \n fiducial region defined in the paper.
Title= Integrated cross-section in extended fiducial region
XLabel= $N_{\mathrm{jet}}$
YLabel= $\sigma^\mathrm{ext.fid.}$ (fb)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d02-x01-y01
#Title= Measured differential cross sections for the $pp\to\nu\bar{nu}\mathrm{\gamma}$ process at\n $\sqrt{s}=13$ TeV as a function of photon $E_{\mathrm T}$ in the inclusive $N_{\mathrm{jets}}\geq0$ extended \n fiducial region defined in the paper.
Title= Inclusive $N_{\mathrm{jets}}\geq0$ region
XLabel= $E_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel= $d\sigma/dE_{\mathrm{T}}^{\gamma}$ (fb/GeV)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d03-x01-y01
#Title= Measured differential cross sections for the $pp\to\nu\bar{nu}\mathrm{\gamma}$ process at\n$\sqrt{s}=13$ TeV as a function of $p^\mathrm{miss}_{\mathrm T}$ in the exclusive $N_{\mathrm{jets}}=0$ \n extended fiducial region defined in the paper.
Title= Exclusive $N_{\mathrm{jets}}=0$ region
XLabel= $E_{\mathrm{T}}^{\gamma}$ (GeV)
YLabel= $d\sigma/dE_{\mathrm{T}}^{\gamma}$ (fb/GeV)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d04-x01-y01
#Title=Measured differential cross sections for the $pp\to\nu\bar{nu}\mathrm{\gamma}$ process at\n$\sqrt{s}=13$ TeV as a function of photon $E_{\mathrm T}$ in the inclusive $N_{\mathrm{jets}}\geq0$ \nextended fiducial region defined in the paper.
Title= Inclusive $N_{\mathrm{jets}}\geq0$ region
XLabel= $p^\mathrm{miss}_{\mathrm{T}}$ (GeV)
YLabel= $d\sigma/dp^\mathrm{miss}_{\mathrm{T}}$ (fb/GeV) 
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d05-x01-y01
#Title= Measured differential cross sections for the $pp\to\nu\bar{nu}\mathrm{\gamma}$ process at\n$\sqrt{s}=13$ TeV as a function of photon $E_{\mathrm T}$ in the exclusive $N_{\mathrm{jets}}=0$ \nextended fiducial region defined in the paper.
Title= Exclusive $N_{\mathrm{jets}}=0$ region
XLabel= $p^\mathrm{miss}_{\mathrm{T}}$ (GeV)
YLabel= $d\sigma/dp^\mathrm{miss}_{\mathrm{T}}$ (fb/GeV)
# END PLOT

# BEGIN PLOT /ATLAS_2018_I1698006/d06-x01-y01
#Title= Measured differential cross sections as a function of $N_{\mathrm{jets}}$ in \nthe extended fiducial region defined in the paper.
Title= Differential cross-section in extended fiducial region
XLabel= $N_{\mathrm{jets}}$
YLabel= $d\sigma/dN_{\mathrm{jets}}$ (fb)
XMinorTickMarks=0
# END PLOT

