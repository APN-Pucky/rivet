# BEGIN PLOT /STAR_2009_UE_HELEN/d01-x01-y01
Title=TransMAX region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
YLabel=$\langle N_\mathrm{ch} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$
LegendTitle={STAR preliminary}
LogY=0
# END PLOT

# BEGIN PLOT /STAR_2009_UE_HELEN/d02-x01-y01
Title=TransMIN region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
YLabel=$\langle N_\mathrm{ch} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$
LegendTitle={STAR preliminary}
LogY=0
# END PLOT

# BEGIN PLOT /STAR_2009_UE_HELEN/d03-x01-y01
Title=Away region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
YLabel=$\langle N_\mathrm{ch} \rangle / \mathrm{jet~area}$
LegendTitle={STAR preliminary}
LogY=0
# END PLOT

