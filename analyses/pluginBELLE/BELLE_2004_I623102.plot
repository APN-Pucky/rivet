BEGIN PLOT /BELLE_2004_I623102/d01-x01-y01
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d02-x01-y01
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($-1<\cos\theta_h<-0.67$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d02-x01-y02
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($-0.67<\cos\theta_h<-0.33$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d02-x01-y03
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($-0.33<\cos\theta_h<0$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d02-x01-y04
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($0.<\cos\theta_h<0.33$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d02-x01-y05
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($0.33<\cos\theta_h<0.67$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d02-x01-y06
Title=Minimum $D^+\pi^-$ mass in $B^-\to D^+\pi^-\pi^-$ ($0.67<\cos\theta_h<1$)
XLabel=$m_{D^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d03-x01-y01
Title=Helicity angle in $B^-\to D^+\pi^-\pi^-$ ($\left(m^\text{min}_{D^+\pi^-}\right)^2<5\,\text{GeV}^2$)
XLabel=$\cos\theta_h$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d03-x01-y02
Title=Helicity angle in $B^-\to D^+\pi^-\pi^-$ ($5<\left(m^\text{min}_{D^+\pi^-}\right)^2<5.9\,\text{GeV}^2$)
XLabel=$\cos\theta_h$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d03-x01-y03
Title=Helicity angle in $B^-\to D^+\pi^-\pi^-$ ($5.9<\left(m^\text{min}_{D^+\pi^-}\right)^2<6.2\,\text{GeV}^2$)
XLabel=$\cos\theta_h$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d03-x01-y04
Title=Helicity angle in $B^-\to D^+\pi^-\pi^-$ ($\left(m^\text{min}_{D^+\pi^-}\right)^2>6.2\,\text{GeV}^2$)
XLabel=$\cos\theta_h$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_h$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d04-x01-y01
Title=Minimum $D^{*+}\pi^-$ mass in $B^-\to D^{*+}\pi^-\pi^-$
XLabel=$m_{D^{*+}\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^{*+}\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d05-x01-y01
Title=Helicity angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.76\,\text{GeV}^2$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x01-y02
Title=Helicity angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.76<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.98\,\text{GeV}^2$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x01-y03
Title=Helicity angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.98<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<6.15\,\text{GeV}^2$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x01-y04
Title=Helicity angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2>6.15\,\text{GeV}^2$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d05-x02-y01
Title=$\alpha$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.76\,\text{GeV}^2$)
XLabel=$\cos\alpha$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x02-y02
Title=$\alpha$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.76<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.98\,\text{GeV}^2$)
XLabel=$\cos\alpha$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x02-y03
Title=$\alpha$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.98<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<6.15\,\text{GeV}^2$)
XLabel=$\cos\alpha$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x02-y04
Title=$\alpha$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2>6.15\,\text{GeV}^2$)
XLabel=$\cos\alpha$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\alpha$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2004_I623102/d05-x03-y01
Title=$\gamma$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.76\,\text{GeV}^2$)
XLabel=$\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\gamma$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x03-y02
Title=$\gamma$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.76<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<5.98\,\text{GeV}^2$)
XLabel=$\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\gamma$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x03-y03
Title=$\gamma$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($5.98<\left(m^\text{min}_{D^{*+}\pi^-}\right)^2<6.15\,\text{GeV}^2$)
XLabel=$\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\gamma$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I623102/d05-x03-y04
Title=$\gamma$ angle in $B^-\to D^{*+}\pi^-\pi^-$ ($\left(m^\text{min}_{D^{*+}\pi^-}\right)^2>6.15\,\text{GeV}^2$)
XLabel=$\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\gamma$
LogY=0
END PLOT
