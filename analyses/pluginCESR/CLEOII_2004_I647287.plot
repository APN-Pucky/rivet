BEGIN PLOT /CLEOII_2004_I647287/d01-x01-y01
Title=$\langle M^2_X-\bar{M}^2_D\rangle$ for $E_\ell>1\,$GeV
YLabel=$\langle M^2_X-\bar{M}^2_D\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x01-y02
Title=$\langle M^2_X-\bar{M}^2_D\rangle$ for $E_\ell>1.5\,$GeV
YLabel=$\langle M^2_X-\bar{M}^2_D\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x02-y01
Title=$\langle(M^2_X-\langle M^2_X\rangle)^2\rangle$ for $E_\ell>1\,$GeV
YLabel=$\langle(M^2_X-\langle M^2_X\rangle)^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x02-y02
Title=$\langle(M^2_X-\langle M^2_X\rangle)^2\rangle$ for $E_\ell>1.5\,$GeV
YLabel=$\langle(M^2_X-\langle M^2_X\rangle)^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x03-y01
Title=$\langle q^2\rangle$ for $E_\ell>1\,$GeV
YLabel=$\langle q^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x03-y02
Title=$\langle q^2\rangle$ for $E_\ell>1.5\,$GeV
YLabel=$\langle q^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x04-y01
Title=$\langle(q^2-\langle q^2\rangle)^2\rangle$ for $E_\ell>1\,$GeV
YLabel=$\langle(q^2-\langle q^2\rangle)^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d01-x04-y02
Title=$\langle(q^2-\langle q^2\rangle)^2\rangle$ for $E_\ell>1.5\,$GeV
YLabel=$\langle(q^2-\langle q^2\rangle)^2\rangle$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647287/d02-x01-y01
Title=$\langle M^2_X-\bar{M}^2_D\rangle$ vs $E^\text{min}_\ell$
YLabel=$\langle M^2_X-\bar{M}^2_D\rangle$ [$\text{GeV}^2$]
XLabel= $E^\text{min}_\ell$ [GeV]
LogY=0
END PLOT
