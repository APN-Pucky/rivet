BEGIN PLOT /BELLE_2007_I748975/d01
XLabel=$m_{p\bar{\Lambda}^0}$ [GeV]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}m_{p\bar{\Lambda}^0}\times10^6$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d01-x01-y01
Title=$p\bar{\Lambda}^0$ mass for $B^+\to p\bar{\Lambda}^0\gamma$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d01-x01-y02
Title=$p\bar{\Lambda}^0$ mass for $B^+\to p\bar{\Lambda}^0\pi^0$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d01-x01-y03
Title=$p\bar{\Lambda}^0$ mass for $B^0\to p\bar{\Lambda}^0\pi^-$
END PLOT

BEGIN PLOT /BELLE_2007_I748975/d02
XLabel=$\cos\theta_p$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_p$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d02-x01-y01
Title=$\cos\theta_p$ for $B^+\to p\bar{\Lambda}^0\gamma$ ($m_{p\bar{\Lambda}^0}<2.8\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d02-x01-y02
Title=$\cos\theta_p$ for $B^+\to p\bar{\Lambda}^0\pi^0$ ($m_{p\bar{\Lambda}^0}<2.8\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d02-x01-y03
Title=$\cos\theta_p$ for $B^0\to p\bar{\Lambda}^0\pi^-$ ($m_{p\bar{\Lambda}^0}<2.8\,$GeV)
END PLOT

BEGIN PLOT /BELLE_2007_I748975/d03
XLabel=
YLabel=$\bar\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d03-x01-y01
Title=$\bar\alpha$ for $B^+\to p\bar{\Lambda}^0\gamma$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d03-x01-y02
Title=$\bar\alpha$ for $B^+\to p\bar{\Lambda}^0\pi^0$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d03-x01-y03
Title=$\bar\alpha$ for $B^0\to p\bar{\Lambda}^0\pi^-$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d04
XLabel=
YLabel=$\bar{E}_\Lambda$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d04-x01-y01
Title=Average $\bar{\Lambda}^0$ energy for $B^+\to p\bar{\Lambda}^0\gamma$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d04-x01-y02
Title=Average $\bar{\Lambda}^0$ energy for $B^+\to p\bar{\Lambda}^0\pi^0$
END PLOT
BEGIN PLOT /BELLE_2007_I748975/d04-x01-y03
Title=Average $\bar{\Lambda}^0$ energy for $B^0\to p\bar{\Lambda}^0\pi^-$
END PLOT
