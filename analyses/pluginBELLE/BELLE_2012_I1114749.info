Name: BELLE_2012_I1114749
Year: 2012
Summary: $e^+e^-\to e^+e^-\pi^0$ via intermediate photons at 10.58 GeV
Experiment: BELLE
Collider: KEKB
InspireID: 1114749
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 86 (2012) 092007
RunInfo: e+ e- > e+e- meson via photon photon -> meson
Beams: [e+, e-]
Energies: [10.58]
Description:
  'Measurement of the cross sections for the production of $\pi^0$ in photon-photon
   collisions, i.e. $e^+e^-\to \gamma\gamma e^+e^-$ followed by $\gamma\gamma\to\pi^0$,
   by the BELLE experiment at 10.58 GeV'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Uehara:2012ag
BibTeX: '@article{Uehara:2012ag,
    author = "Uehara, S. and others",
    collaboration = "Belle",
    title = "{Measurement of $\gamma \gamma^* \to \pi^0$ transition form factor at Belle}",
    eprint = "1205.3249",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BELLE-PREPRINT-2012-16, KEK-PREPRINT-2012-8",
    doi = "10.1103/PhysRevD.86.092007",
    journal = "Phys. Rev. D",
    volume = "86",
    pages = "092007",
    year = "2012"
}'
