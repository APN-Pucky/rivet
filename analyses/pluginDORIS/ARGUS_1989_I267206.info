Name: ARGUS_1989_I267206
Year: 1989
Summary: $\bar{B}^0\to D^{*+}\ell^-\bar\nu_\ell$
Experiment: ARGUS
Collider: DORIS
InspireID: 267206
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 219 (1989) 121-126
RunInfo: Any process producing B mesons, originally Upsilon(4S) decays
Description:
  '$q^2$ spectrum, lepton momentum spectrum and $D^*$ helicty angle in $\bar{B}^0\to D^{*+}\ell^-\bar\nu_\ell$. Corrected data read from figure 7.'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: ARGUS:1988frj
BibTeX: '@article{ARGUS:1988frj,
    author = "Albrecht, H. and others",
    collaboration = "ARGUS",
    title = "{Measurement of D*+ Polarization in the Decay anti-B0 ---\ensuremath{>} D*+ L- anti-neutrino}",
    reportNumber = "DESY-88-178",
    doi = "10.1016/0370-2693(89)90851-4",
    journal = "Phys. Lett. B",
    volume = "219",
    pages = "121--126",
    year = "1989"
}
'
