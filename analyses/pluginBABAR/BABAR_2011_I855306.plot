BEGIN PLOT /BABAR_2011_I855306/d01-x01-y01
Title=$B^0\to \pi^-\ell^+\nu$ (1 mode)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^4 \mathrm{d}\mathrm{Br}(B^+\to B^0\to \pi^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I855306/d01-x01-y02
Title=$B^+\to \pi^0\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^4 \mathrm{d}\mathrm{Br}(B^+\to B^0\to \pi^0\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I855306/d01-x01-y03
Title=$B^0\to \pi^-\ell^+\nu$ (4 modes)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{4} \mathrm{d}\mathrm{Br}(B^+\to B^0\to \pi^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I855306/d02-x01-y01
Title=$B^0\to \rho^-\ell^+\nu$ (1 mode)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{4} \mathrm{d}\mathrm{Br}(B^+\to B^0\to \rho^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I855306/d02-x01-y02
Title=$B^+\to \rho^0\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{4} \mathrm{d}\mathrm{Br}(B^+\to B^0\to \rho^0\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I855306/d02-x01-y03
Title=$B^0\to \rho^-\ell^+\nu$ (4 modes)
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{4} \mathrm{d}\mathrm{Br}(B^+\to B^0\to \rho^-\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
