BEGIN PLOT /ARGUS_1993_I357133/d01-x01-y01
Title=Primary electron momentum spectrum
XLabel=$p_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1993_I357133/d02-x01-y01
Title=Secondary secondary momentum spectrum
XLabel=$p_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
