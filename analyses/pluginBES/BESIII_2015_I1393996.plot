BEGIN PLOT /BESIII_2015_I1393996/d01-x01-y01
Title=Cross section for $e^+e^-\to Z_c(3885)^0\pi^0\to(DD^*)\pi^0$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1393996/d02-x01-y02
Title=Mass distribution for $D^+D^{*-}$ at $\sqrt{s}=4.226$ GeV
XLabel=$m_{D^+D^{*-}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+D^{*-}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1393996/d02-x01-y01
Title=Mass distribution for $D^+D^{*-}$ at $\sqrt{s}=4.257$ GeV
XLabel=$m_{D^+D^{*-}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^+D^{*-}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1393996/d02-x02-y02
Title=Mass distribution for $D^0D^{*0}$ at $\sqrt{s}=4.226$ GeV
XLabel=$m_{D^0D^{*0}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0D^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1393996/d02-x02-y01
Title=Mass distribution for $D^0D^{*0}$ at $\sqrt{s}=4.257$ GeV
XLabel=$m_{D^0D^{*0}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0D^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1393996/d03-x01-y01
Title=Mass distribution for $DD^*$ average over modes and $\sqrt{s}$
XLabel=$m_{DD^*}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{DD^*}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
