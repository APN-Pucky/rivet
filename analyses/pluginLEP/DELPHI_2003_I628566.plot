BEGIN PLOT /DELPHI_2003_I628566/d01-x01-y01
Title=$f_1(1285)$ multiplicity
YLabel=$\langle n\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2003_I628566/d01-x01-y02
Title=$f^\prime_1(1420)$ multiplicity
YLabel=$\langle n\rangle$
LogY=0
END PLOT
BEGIN PLOT /DELPHI_2003_I628566/d02-x01-y01
Title=$f_1(1285)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
END PLOT
BEGIN PLOT /DELPHI_2003_I628566/d02-x01-y02
Title=$f_1^\prime(1420)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
END PLOT
