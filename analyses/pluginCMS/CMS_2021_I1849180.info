Name: CMS_2021_I1849180
Year: 2021
Summary: Drell-Yan dimuon production in proton-lead collisions at 8.16 TeV
Experiment: CMS
Collider: LHC
InspireID: 1849180
Status: VALIDATED
Reentrant: True
Authors:
 - Hannes Jung <hannes.jung@desy.de>
 - Sara Taheri Monfared <taheri@mail.desy.de>
 - Mohammad Hossein Karam Sichani <mohammad.hossein.karam.sichani@cern.ch>
References:
 - CMS-HIN-18-003
 - JHEP 05 (2021) 182
 - arXiv:2102.13648
RunInfo: DY events with m(ll)>15 GeV, run in pPb mode
Beams: [p+, Pb]
Energies: [[6500, 2560], [4080, 4080]]
Luminosity_nb: 173.0
Description: 'The differential cross-section for Drell-Yan dimuon production in proton-lead collisions at 8.16 TeV have been measured with data acquired by the CMS collaboration at the LHC during the year 2016.'
Keywords: []
BibKey: CMS:2021ynu
BibTeX: '@article{CMS:2021ynu,
    author = "Sirunyan, Albert M and others",
    collaboration = "CMS",
    title = "{Study of Drell-Yan dimuon production in proton-lead collisions at $\sqrt{s_\mathrm{NN}} =$ 8.16 TeV}",
    eprint = "2102.13648",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CMS-HIN-18-003, CERN-EP-2021-028",
    doi = "10.1007/JHEP05(2021)182",
    journal = "JHEP",
    volume = "05",
    pages = "182",
    year = "2021"
}'
