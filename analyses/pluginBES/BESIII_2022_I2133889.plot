BEGIN PLOT /BESIII_2022_I2133889/
XLabel=$\sqrt{s}$/GeV
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d01-x01-y01
Title=$\sigma(e^+e^-\to \omega\pi^+\pi^-)$
YLabel=$\sigma(e^+e^-\to \omega\pi^+\pi^-)$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y01
Title=$\sigma(e^+e^-\to \omega f_0(500))$
YLabel=$\sigma(e^+e^-\to \omega f_0(500))$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y02
Title=$\sigma(e^+e^-\to \omega f_0(980))$
YLabel=$\sigma(e^+e^-\to \omega f_0(980))$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y03
Title=$\sigma(e^+e^-\to \omega f_0(1370))$
YLabel=$\sigma(e^+e^-\to \omega f_0(1370))$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y04
Title=$\sigma(e^+e^-\to \omega f_2(1270))$
YLabel=$\sigma(e^+e^-\to \omega f_2(1270))$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y05
Title=$\sigma(e^+e^-\to b_1(1235)^\pm\pi^\mp)$
YLabel=$\sigma(e^+e^-\to b_1(1235)^\pm\pi^\mp)$/pb
END PLOT
BEGIN PLOT /BESIII_2022_I2133889/d02-x01-y06
Title=Non-resonant $\sigma(e^+e^-\to \omega\pi^+\pi^-)$
YLabel=$\sigma(e^+e^-\to \omega\pi^+\pi^-)$/pb
END PLOT
