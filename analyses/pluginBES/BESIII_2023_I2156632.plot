BEGIN PLOT /BESIII_2023_I2156632/d01
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2156632/d01-x01-y01
Title=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$ (using $J/\psi\to e^+e^-$)
END PLOT
BEGIN PLOT /BESIII_2023_I2156632/d01-x01-y02
Title=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$ (using $J/\psi\to\mu^+\mu^-$)
END PLOT
BEGIN PLOT /BESIII_2023_I2156632/d01-x01-y03
Title=$\sigma(e^+e^-\to \pi^+\pi^-J/\psi)$ (combined)
END PLOT
