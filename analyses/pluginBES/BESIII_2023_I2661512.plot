BEGIN PLOT /BESIII_2023_I2661512
Title=$\sigma(e^+e^-\to\Delta^{++}\bar{p}\pi^-+\text{c.c})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to\Delta^{++}\bar{p}\pi^-+\text{c.c})$/pb
LogY=0
ConnectGaps=1
END PLOT
