BEGIN PLOT /BESIII_2023_I2702520/d01-x01-y01
Title=$\sigma(e^+e^-\to K^0_SK^0_L\pi^0)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^0_SK^0_L\pi^0)$ [GeV]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2702520/d02-x01-y01
Title=$\sigma(e^+e^-\to K^{*0}\bar{K}^0+\text{c.c.})$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^{*0}\bar{K}^0+\text{c.c.})$ [GeV]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2702520/d03-x01-y01
Title=$\sigma(e^+e^-\to K^{*0}_2\bar{K}^0+\text{c.c.})$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^{*0}_2\bar{K}^0+\text{c.c.})$ [GeV]
ConnectGaps=1
END PLOT