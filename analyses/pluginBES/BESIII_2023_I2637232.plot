BEGIN PLOT /BESIII_2023_I2637232/d01-x01-y01
Title=$K^0_S$ momentum spectrum in $D^+\to K^0_SX$
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ [$\text{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2637232/d01-x01-y02
Title=$K^0_S$ momentum spectrum in $D^0\to K^0_SX$
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ [$\text{GeV}^{-1}$
LogY=0
END PLOT
