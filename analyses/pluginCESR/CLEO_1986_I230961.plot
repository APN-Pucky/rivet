BEGIN PLOT /CLEO_1986_I230961/d02-x01-y01
Title=$J/\psi$ momentum in $\Upsilon(4S)$ decays
XLabel=$|p|$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|p|$ [$\text{GeV}^{1-}$]
LogY=0
END PLOT
