Name: BESIII_2023_I2155157
Year: 2023
Summary:  Mass and angular distributions in $J/\psi\to\gamma K^0_SK^0_S\pi^0$ decays
Experiment: BESIII
Collider: BEPC
InspireID: 2155157
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 115 (2015) 9, 091803
RunInfo: e+e- > J/psi
NeedCrossSection: no
Beams: [e-, e+]
Energies: [3.1]
Description:
  'Measurement of mass and angular distributions in $J/\psi\to\gamma K^0_SK^0_S\pi^0$ decays.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022chl
BibTeX: '@article{BESIII:2022chl,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Study of \ensuremath{\eta}(1405)/\ensuremath{\eta}(1475) in $ J/\psi \to \gamma {K}_S^0{K}_S^0{\pi}^0 $ decay}",
    eprint = "2209.11175",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP03(2023)121",
    journal = "JHEP",
    volume = "03",
    pages = "121",
    year = "2023"
}
'
