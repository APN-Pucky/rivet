// -*- C++ -*-
#ifndef RIVET_Jet_FHH
#define RIVET_Jet_FHH

#include "Rivet/Tools/RivetSTL.hh"
#include "Rivet/Tools/RivetFastJet.hh"
#include "Rivet/Math/Vectors.hh"

namespace Rivet {


  /// @name Jet declarations
  /// @{

  // Forward declarations
  class Jet;
  class Jets;


  /// @name Jet function/functor declarations
  /// @{

  /// std::function instantiation for functors taking a Jet and returning a bool
  using JetSelector = function<bool(const Jet&)>;

  /// std::function instantiation for functors taking two Jets and returning a bool
  using JetSorter = function<bool(const Jet&, const Jet&)>;

  /// @}


  /// Enum for available jet algorithms
  enum class JetAlg { KT=0,
		      AKT=1, ANTIKT=1,
		      CA=2, CAM=2, CAMBRIDGE=2,
		      SISCONE, PXCONE,
		      ATLASCONE, CMSCONE,
		      CDFJETCLU, CDFMIDPOINT, D0ILCONE,
		      JADE, DURHAM, TRACKJET, GENKTEE ,
		      KTET, ANTIKTET };
  

}

#endif
