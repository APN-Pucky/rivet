BEGIN PLOT /BABAR_2008_I791879/d01-x01-y01
Title=Ratio of $m_{\Lambda_c^+\bar{p}^-}$ distribution to phase-space for $B^-\to\Lambda_c^+\bar{p}\pi^-$
XLabel=$m_{\Lambda_c^+\bar{p}^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\Lambda_c^+\bar{p}^-}/\text{PHSP}$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I791879/d02-x01-y01
Title=Helicity angle distribution in $B^-\to\Lambda_c^+\bar{p}\pi^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
