BEGIN PLOT /BESIII_2024_I2819140/d01-x01-y01
Title=$D^0\to K^- e^+ \nu_e$
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2024_I2819140/d02-x01-y01
Title=$D^0\to K^- \mu^+ \nu_\mu$
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2024_I2819140/d03-x01-y01
Title=$D^+\to \bar{K}^0 e^+ \nu_e$
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2024_I2819140/d04-x01-y01
Title=$D^+\to \bar{K}^0 \mu^+ \nu_\mu$
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT
