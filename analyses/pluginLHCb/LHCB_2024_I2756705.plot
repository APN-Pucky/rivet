BEGIN PLOT /LHCB_2024_I2756705/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $B_c^+\to J/\psi \pi^+\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
