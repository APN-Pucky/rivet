BEGIN PLOT /CMS_2022_I2142341/d01-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$p_T^{\gamma\gamma}$ [GeV]
YLabel=$\Delta \sigma / \Delta p_T^{\gamma\gamma}$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d03-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$n_{\mathrm{jets}}$
YLabel=$\Delta \sigma / \Delta n_{\mathrm{jets}}$ [fb]
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d05-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$|\cos\theta^*|$
YLabel=$\Delta \sigma / \Delta |\cos\theta^{*}|$ [fb]
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d07-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$|y^{\gamma\gamma}|$
YLabel=$\Delta \sigma/ \Delta |y^{\gamma\gamma}|$ [fb]
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d09-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$p_{T}^{j_1}$ [GeV]
YLabel=$\Delta \sigma / \Delta p_{T}^{j_1}$ [fb/GeV]
LogY=0
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d11-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$|y^{j_1}|$
YLabel=$\Delta \sigma / \Delta |y^{j_1}|$ [fb]
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d23-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$|\Delta\phi_{j_1, j_2}|$
YLabel=$\Delta \sigma / \Delta |\Delta\phi_{j_1, j_2}|$ [fb]
LogY=0
END PLOT
BEGIN PLOT /CMS_2022_I2142341/d29-x01-y01
Title=CMS, 13 TeV, $H \rightarrow \gamma \gamma$
XLabel=$|\Delta\eta_{j_1, j_2}|$
YLabel=$\Delta \sigma / \Delta |\Delta\eta_{j_1, j_2}|$ [fb]
LogY=0
END PLOT

