BEGIN PLOT /BESIII_2015_I1356733/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^+\pi^-X(3823)(\to\gamma\chi_{c1})$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^+\pi^-\pi^+\pi^-X(3823)(\to\gamma\chi_{c1}) )$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2015_I1356733/d01-x01-y02
Title=Cross section for $e^+e^-\to\pi^+\pi^- \psi(2S)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^+\pi^- \psi(2S))$ [pb]
LogY=0
ConnectGaps=1
END PLOT
