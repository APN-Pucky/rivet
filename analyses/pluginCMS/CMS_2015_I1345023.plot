BEGIN PLOT /CMS_2015_I1345023/d01-x01-y01
END PLOT

BEGIN PLOT /CMS_2015_I1345023/d01-x01-y01
Title=$J/\psi$ transverse momentum $0 < |y| < 0.3$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d01-x01-y02
Title=$J/\psi$ transverse momentum $0.3 < |y| < 0.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d01-x01-y03
Title=$J/\psi$ transverse momentum $0.6 < |y| < 0.9$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d01-x01-y04
Title=$J/\psi$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/as\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d03-x01-y01
Title=$J/\psi$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2015_I1345023/d02-x01-y01
Title=$\psi(2S)$ transverse momentum $0 < |y| < 0.3$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d02-x01-y02
Title=$\psi(2S)$ transverse momentum $0.3 < |y| < 0.6$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d02-x01-y03
Title=$\psi(2S)$ transverse momentum $0.6 < |y| < 0.9$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d02-x01-y04
Title=$\psi(2S)$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1345023/d04-x01-y01
Title=$\psi(2S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2015_I1345023/d05-x01-y01
Title=Ratio of $\psi(2S)$ to $J/\psi$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$R_{21}$ [$\%$]
LogY=0
END PLOT