BEGIN PLOT /BESII_2004_I622224/d0
XLabel=$p$ [GeV]
YLabel=$\frac{1}{N}\frac{\mathrm{d}N}{\mathrm{d}p}$ [$\mathrm{GeV}^{-1}$]
LogY=1
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2004_I622224/d0[1,2,3,4,5,6]-x01-y01
XLabel=$\xi$
YLabel=$\frac{1}{N}\frac{\mathrm{d}N}{\mathrm{d}\xi}$
LogY=1
ConnectGaps=1
END PLOT


BEGIN PLOT /BESII_2004_I622224/d0[1,7]-x01-y01
Title=Charged Particle Momentum at 2.2 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d0[2,8]-x01-y01
Title=Charged Particle Momentum at 2.6 GeV
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2004_I622224/d0[3,9]-x01-y01
Title=Charged Particle Momentum at 3.0 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d04-x01-y01
Title=Charged Particle Momentum at 3.2 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d10-x01-y01
Title=Charged Particle Momentum at 3.2 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d05-x01-y01
Title=Charged Particle Momentum at 4.6 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d11-x01-y01
Title=Charged Particle Momentum at 4.6 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d06-x01-y01
Title=Charged Particle Momentum at 4.8 GeV
END PLOT
BEGIN PLOT /BESII_2004_I622224/d12-x01-y01
Title=Charged Particle Momentum at 4.8 GeV
END PLOT