BEGIN PLOT /BELLE_2012_I1123656/d01-x01-y01
Title=$\cos\theta_\text{tr}$ in $B^0\to D^{*+}D^{*-}$
XLabel=$\cos\theta_\text{tr}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\text{tr}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2012_I1123656/d01-x01-y02
Title=$\cos\theta_1$ in $B^0\to D^{*+}D^{*-}$
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_1$
LogY=0
END PLOT
