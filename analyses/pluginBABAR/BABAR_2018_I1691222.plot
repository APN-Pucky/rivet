BEGIN PLOT /BABAR_2018_I1691222/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta^\prime$
XLabel=$Q_{1,2}^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q_1^2/\text{d}Q_1^2$ [$\text{fb}/\text{GeV}^4$]
END PLOT
