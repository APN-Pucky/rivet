Name: BELLE_2005_I679165
Year: 2005
Summary: Differential branching ratios in $B^+\to p\bar{p}K^+$, $B^0\to p\bar{p}K^0_S$ and $B^0\to p\bar{\Lambda}\pi^-$
Experiment: BELLE
Collider: KEKB
InspireID: 679165
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 617 (2005) 141-149
RunInfo: Any process producing B+ mesons, originally Upsilon(4S) decays 
Description:
  'Differential branching ratios in $B^+\to p\bar{p}K^+$, $B^0\to p\bar{p}K^0_S$ and $B^0\to p\bar{\Lambda}\pi^-$.The corrected data was read from the figures in the paper.'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2005mke
BibTeX: '@article{Belle:2005mke,
    author = "Wang, M. -Z. and others",
    collaboration = "Belle",
    title = "{Study of the baryon-antibaryon low-mass enhancements in charmless three-body baryonic B decays}",
    eprint = "hep-ex/0503047",
    archivePrefix = "arXiv",
    reportNumber = "BELLE-PREPRINT-2005-13, KEK-PREPRINT-2005-4",
    doi = "10.1016/j.physletb.2005.05.008",
    journal = "Phys. Lett. B",
    volume = "617",
    pages = "141--149",
    year = "2005"
}
'
