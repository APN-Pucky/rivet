BEGIN PLOT /BABAR_2007_I722710/d01-x01-y01
Title=$K^+\pi^-$ mass in $B^0\to K^{*0}(\to K^+\pi^-) \eta^\prime$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I722710/d02-x01-y01
Title=Helicity angle in $B^0\to K^{*0}(\to K^+\pi^-) \eta^\prime$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
