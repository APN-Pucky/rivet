BEGIN PLOT /BESIII_2022_I2122399/d01-x01-y01
Title=$K^-p$ mass in $\Lambda_c^+\to K^-p e^+\nu_e$
XLabel=$m_{K^-p}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^-p}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
