BEGIN PLOT /LHCB_2013_I1230344/d02-x01-y01
Title=Prompt J$/\psi$ production
XLabel=y
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d02-x01-y02
Title=Non-Prompt J$/\psi$ production
XLabel=y
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d0[3,4,5,6,7]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d[11,12,13,14,15]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d0[3,4,5,6,7]-x01-y03
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction [percent]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d05-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d05-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d06-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d07-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d03-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d04-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d05-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d06-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d07-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($4.0<y<4.5$) 
END PLOT


BEGIN PLOT /LHCB_2013_I1230344/d09-x01-y01
Title=$\Upsilon(1S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d09-x01-y02
Title=$\Upsilon(2S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d09-x01-y03
Title=$\Upsilon(3S)$ production
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d10-x01-y01
Title=$\Upsilon(1S)$ production
XLabel=y
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d10-x01-y02
Title=$\Upsilon(2S)$ production
XLabel=y
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d10-x01-y03
Title=$\Upsilon(3S)$ production
XLabel=y
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d11-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d12-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d13-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d14-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d15-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d11-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d12-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d13-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d14-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d15-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d11-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d12-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d13-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d14-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d15-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d16-x01-y01
Title=$\Upsilon(2S)$/$\Upsilon(1S)$ ratio
XLabel=$p_\perp$ [GeV]
YLabel=$N_(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d16-x01-y02
Title=$\Upsilon(3S)$/$\Upsilon(1S)$ ratio
XLabel=$p_\perp$ [GeV]
YLabel=$N_(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
LogY=0
END PLOT

BEGIN PLOT /LHCB_2013_I1230344/d17-x01-y01
Title=$\Upsilon(2S)$/$\Upsilon(1S)$ ratio
XLabel=$y$
YLabel=$N_(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2013_I1230344/d17-x01-y02
Title=$\Upsilon(3S)$/$\Upsilon(1S)$ ratio
XLabel=$y$
YLabel=$N_(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
LogY=0
END PLOT
