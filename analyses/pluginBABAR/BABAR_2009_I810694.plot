BEGIN PLOT /BABAR_2009_I810694/d01-x01-y01
Title=Minimum $m^2_{D^+\pi^-}$ mass squared in $B^-\to D^+\pi^-\pi^-$
XLabel=$m^2_{D^+\pi^-,\min}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{D^+\pi^-,\min}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I810694/d01-x01-y02
Title=Maximum $m^2_{D^+\pi^-}$ mass squared in $B^-\to D^+\pi^-\pi^-$
XLabel=$m^2_{D^+\pi^-,\max}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{D^+\pi^-,\max}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I810694/d01-x01-y03
Title=$m^2_{\pi^-\pi^-}$ mass squared in $B^-\to D^+\pi^-\pi^-$
XLabel=$m^2_{\pi^-\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^-\pi^-}$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I810694/d02-x01-y01
Title=Helicity angle for $4.5<m^2_{D^+\pi^-}<5.5\,\text{GeV}^2$ in $B^-\to D^+\pi^-\pi^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I810694/d02-x01-y02
Title=Helicity angle for $5.9<m^2_{D^+\pi^-}<6.2\,\text{GeV}^2$ in $B^-\to D^+\pi^-\pi^-$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta\cos\theta$
LogY=0
END PLOT
