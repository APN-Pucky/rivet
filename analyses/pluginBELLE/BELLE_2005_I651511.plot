BEGIN PLOT /BELLE_2005_I651511/d01-x01-y01
Title=Helicity angle in $B^+\to\pi^+\pi^0\pi^0$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I651511/d02-x01-y01
Title=$\pi^+\pi^0$ mass in $B^+\to\pi^+\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
