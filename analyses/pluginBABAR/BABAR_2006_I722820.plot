BEGIN PLOT /BABAR_2006_I722820/d01-x01-y01
Title=$K\pi$ mass in $B^0\to K\pi \eta$
XLabel=$m_{K\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722820/d01-x01-y02
Title=$K\pi$ mass in $B^+\to K\pi \eta$
XLabel=$m_{K\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2006_I722820/d01-x02-y01
Title=Helicity angle in $B^0\to K\pi \eta$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I722820/d01-x02-y02
Title=Helicity angle in $B^+\to K\pi \eta$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
