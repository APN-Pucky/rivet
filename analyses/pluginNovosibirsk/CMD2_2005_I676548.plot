BEGIN PLOT /CMD2_2005_I676548/d02-x01-y01
Title=Differential $\omega\to e^+e^-\pi^0$ decay
XLabel=$m_{e^+e^-}$ [MeV]
YLabel=$\left|F_{\omega\pi^0}\left(m^2_{e^+e^-}\right)\right|^2$
LogY=0
END PLOT
