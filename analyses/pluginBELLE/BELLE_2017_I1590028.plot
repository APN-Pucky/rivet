BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y01
Title=$D\bar{D}$ mass in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$m_{D\bar{D}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D\bar{D}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y02
Title=$J/\psi$ production angle in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$\cos\theta_{\text{prod}}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{\text{prod}}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y03
Title=$J/\psi$ helicity angle in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$\cos\theta_{J/\psi}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{J/\psi}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y04
Title=$\ell^-$ azimuthal angle in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$\phi_\ell$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\ell$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y05
Title=$X$ helicity angle in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$\cos\theta_{X}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{X}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2017_I1590028/d01-x01-y06
Title=$D$ azimuthal angle in $e^+e^-\to D\bar{D}J/\psi$ at $\sqrt{s}=10.58$GeV
XLabel=$\phi_D$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_D$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2017_I1590028/d02-x01-y01
Title=Cross Section for $e^+e^-\to J/\psi X^*(3860)(\to D\bar{D})$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [fb]
LogY=0
END PLOT