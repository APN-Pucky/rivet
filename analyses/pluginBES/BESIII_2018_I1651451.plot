BEGIN PLOT /BESIII_2018_I1651451/d01-x01-y01
Title=$\sigma(e^+e^-\to \phi\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \phi\pi^+\pi^-)$/pb
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1651451/d01-x01-y02
Title=$\sigma(e^+e^-\to \phi\pi^0\pi^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \phi\pi^0\pi^0)$/pb
ConnectGaps=1
LogY=0
END PLOT
