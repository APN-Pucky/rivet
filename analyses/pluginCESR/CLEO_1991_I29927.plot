BEGIN PLOT /CLEO_1991_I29927/d01-x01-y01
Title=Cross Section for $b\bar{b}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma_B$ [pb]
ConnectGaps=1
END PLOT

BEGIN PLOT /CLEO_1991_I29927/d02-x01-y01
Title=Cross Section for $B^*$ production
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma_B$ [pb]
ConnectGaps=1
END PLOT
