BEGIN PLOT /BESIII_2015_I1391138/d01-x01-y03
Title=$D^0\to K^- e^+ \nu_e$
XLabel=$q^2/$GeV$^2$
YLabel=$\Delta\Gamma/$ns$^{-1}$
XCustomMajorTicks=1 $0.0-0.1$ 2 $0.1-0.2$ 3 $0.2-0.3$ 4 $0.3-0.4$ 5 $0.4-0.5$ 6 $0.5-0.6$ 7 $0.6-0.7$ 8 $0.7-0.8$ 9 $0.8-0.9$ 10 $0.9-1.0$ 11 $1.0-1.1$ 12 $1.1-1.2$ 13 $1.2-1.3$ 14 $1.3-1.4$ 15 $1.4-1.5$ 16 $1.5-1.6$ 17 $1.6-1.7$ 18 $1.7-q^2_{\max}$
XMajorTicksAngle=60
END PLOT
BEGIN PLOT /BESIII_2015_I1391138/d02-x01-y03
Title=$D^0\to \pi^- e^+ \nu_e$
XLabel=$q^2/$GeV$^2$
YLabel=$\Delta\Gamma/$ns$^{-1}$
XCustomMajorTicks=1 $0.0-0.2$ 2 $0.2-0.4$ 3 $0.4-0.6$ 4 $0.6-0.8$ 5 $0.8-1.0$ 6 $1.0-1.2$ 7 $1.2-1.4$ 8 $1.4-1.6$ 9 $1.6-1.8$ 10 $1.8-2.0$ 11 $2.0-2.2$ 12 $2.2-2.4$ 13 $2.4-2.6$ 14 $2.6-q^2_{\max}$
XMajorTicksAngle=60
END PLOT


