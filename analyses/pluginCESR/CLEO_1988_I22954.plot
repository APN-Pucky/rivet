BEGIN PLOT /CLEO_1988_I22954
XLabel=$x^+$
LogY=0
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d0[1,3,5,7,9]
YLabel=$\mathcal{B}\times\text{d}\sigma/\text{d}x^+$ [pb]
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d[10,11]
YLabel=$\mathcal{B}\times\text{d}\sigma/\text{d}x^+$ [pb]
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d0[2,4,6,8]
YLabel=$s\times\text{d}\sigma/\text{d}x^+$ [$\text{nb}\text{GeV}^2$]
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13
YLabel=$\sigma\times{B}$ [pb]
LogY=0
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d14
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
END PLOT

BEGIN PLOT /CLEO_1988_I22954/d01-x01-y01
Title=Differential cross section for $D^{*+}\to D^0(\to K^-\pi^+) \pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d0[2,4]-x01-y01
Title=Differential cross section for $D^{*+}$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d03-x01-y01
Title=Differential cross section for $D^{*+}\to D^0(\to K^-\pi^+\pi^+\pi^-) \pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d05-x01-y01
Title=Differential cross section for $D^0\to K^-\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d06-x01-y01
Title=Differential cross section for $D^0$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d07-x01-y01
Title=Differential cross section for $D^+\to K^-\pi^+\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d08-x01-y01
Title=Differential cross section for $D^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d09-x01-y01
Title=Differential cross section for $D^+_s\to \phi\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d10-x01-y01
Title=Differential cross section for $\Lambda^+_c\to p K^-\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d11-x01-y01
Title=Differential cross section for $D^{*0}\to D^0(\to K^-\pi^+) (\pi^0,\gamma)$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d12-x01-y01
Title=Differential cross section for $D^{*0}$
YLabel=$s\times\text{d}\sigma/\text{d}x^+$ [$\text{nb}\text{GeV}^2$]
END PLOT

BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y01
Title=Cross section for $D^0\to K^-\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y02
Title=Cross section for $D^+\to K^-\pi^+\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y03
Title=Cross section for $D^{*+}\to D^0(\to K^-\pi^+) \pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y04
Title=Cross section for $D^{*+}\to D^0(\to K^-\pi^+\pi^+\pi^-) \pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y05
Title=Cross section for $D^{*0}\to D^0(\to K^-\pi^+) (\pi^0,\gamma)$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y06
Title=Cross section for $D^+_s\to \phi\pi^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d13-x0[1,2]-y07
Title=Cross section for $\Lambda^+_c\to p K^-\pi^+$
END PLOT

BEGIN PLOT /CLEO_1988_I22954/d14-x01-y01
Title=Cross section for $D^0$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d14-x01-y02
Title=Cross section for $D^+$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d14-x01-y03
Title=Cross section for $D^{*+}$
END PLOT
BEGIN PLOT /CLEO_1988_I22954/d14-x01-y04
Title=Cross section for $D^{*0}$
END PLOT
