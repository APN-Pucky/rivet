BEGIN PLOT /LHCB_2013_I1215607/d01-x01-y01
XLabel=$p_\perp$ [GeV]
YLabel=$f_{s}/f_d$
Title=$B_s^0$ fraction
LogY=0
END PLOT
BEGIN PLOT /LHCB_2013_I1215607/d02-x01-y01
XLabel=$y$
YLabel=$f_{s}/f_d$
Title=$B_s^0$ fraction
LogY=0
END PLOT
