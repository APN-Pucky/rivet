Name: BESIII_2023_I2627838
Year: 2023
Summary: Helicity amplitudes in $\chi_{cJ}\to \phi\phi$ decays.
Experiment: BESIII
Collider: BEPC
InspireID: 2627838
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 05 (2023) 069
 - Phys.Rev.D 88 (2013) 3, 034025
RunInfo: Any process producing psi(2S) to gamma chi_c decays (originally e+e-)
Description:
'Measurement of the ratios of helicity amplitudes in $\chi_{cJ}\to \phi\phi$ decays, ($J=0,1,2$).
 The ratios are extracted using appropriate moments'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023zcs
BibTeX: '@article{BESIII:2023zcs,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Helicity amplitude analysis of \ensuremath{\chi}$_{cJ}$\textrightarrow{} \ensuremath{\phi}\ensuremath{\phi}}",
    eprint = "2301.12922",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1007/JHEP05(2023)069",
    journal = "JHEP",
    volume = "05",
    pages = "069",
    year = "2023"
}
'
