BEGIN PLOT /CLEOC_2007_I761696/d01-x01-y01
Title=$\sigma(e^+e^-\to D^0\bar{D}^0)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to D^0\bar{D}^0)$/nb
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2007_I761696/d01-x01-y02
Title=$\sigma(e^+e^-\to D^+D^-)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to D^+D^-)$/nb
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2007_I761696/d01-x01-y03
Title=$\sigma(e^+e^-\to D\bar{D})$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to D\bar{D})$/nb
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /CLEOC_2007_I761696/d01-x01-y04
Title=$\sigma(e^+e^-\to D^+D^-)/\sigma(e^+e^-\to D^0\bar{D}^0)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to D^+D^-)/\sigma(e^+e^-\to D^0\bar{D}^0)$
ConnectGaps=1
LogY=0
END PLOT