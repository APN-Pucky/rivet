BEGIN PLOT /CLEOII_1997_I439745/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \Lambda^0\bar\Lambda^0$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \Lambda^0\bar\Lambda^0)$ [nb]
LogY=1
END PLOT

BEGIN PLOT /CLEOII_1997_I439745/d02-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-(\Lambda^0/\Sigma^0)(\bar\Lambda^0/\bar\Sigma^0)$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to e^+e^-(\Lambda^0/\Sigma^0)(\bar\Lambda^0/\bar\Sigma^0))$ [nb]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1997_I439745/d03-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\Lambda^0\bar\Lambda^0$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to e^+e^-\Lambda^0\bar\Lambda^0)$ [nb]
LogY=0
END PLOT
