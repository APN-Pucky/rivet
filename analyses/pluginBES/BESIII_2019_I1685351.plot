BEGIN PLOT /BESIII_2019_I1685351/d01-x01-y01
Title=$\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to \mu^+\mu^-)$/nb
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2019_I1685351/d01-x01-y02
Title=$\sigma(e^+e^-\to \eta\pi^+\pi^-)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to \eta\pi^+\pi^-)$/nb
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2019_I1685351/d01-x01-y03
Title=$\sigma(e^+e^-\to 2\pi^+2\pi^-\pi^0)$
XLabel=$\sqrt{s}$/MeV
YLabel=$\sigma(e^+e^-\to 2\pi^+2\pi^-\pi^0)$/nb
LogY=0
ConnectGaps=1
END PLOT
