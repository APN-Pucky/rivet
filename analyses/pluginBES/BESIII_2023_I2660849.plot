BEGIN PLOT /BESIII_2023_I2660849/d01-x01-y01
Title=$e^+e^-\to D^{*+}_sD^{*-}_s$ cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to D^{*+}_sD^{*-}_s)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
