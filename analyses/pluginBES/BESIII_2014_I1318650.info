Name: BESIII_2014_I1318650
Year: 2014
Summary: Cross section for $e^+e^-\to\pi^0\pi^0 h_c$ for $\sqrt{s}=4.23$, $4.26$ and $4.30$ GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1318650
Status: VALIDATED NOHEPDATA SINGLEWEIGHT
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 113 (2014) 21, 212002
RunInfo: e+ e- to hadrons, pi0 set stable
Beams: [e+, e-]
Energies: [4,23,4.26,4.36]
Options:
- PID=*
- ENERGY=*
Description:
'Measurement of the cross section for $e^+e^-\to\pi^0\pi^0 h_c$ for $\sqrt{s}=4.23$, $4.26$ and $4.30$ GeV.
The cross section for $e^+e^-\to Z_xc(4020)^0\pi^0\to \pi^0\pi^0 h_c$ is also measured.
As there is no PDG code for the $Z_c(4020)^0$ we take it to be the first unused exotic $c\bar{c}$ value 9030443,
although this can be changed using the PID option.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2014gnk
BibTeX: '@article{BESIII:2014gnk,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of $e^+e^- → π^0π^0h_c$ and a Neutral Charmoniumlike Structure $Z_c(4020)^0$}",
    eprint = "1409.6577",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.113.212002",
    journal = "Phys. Rev. Lett.",
    volume = "113",
    number = "21",
    pages = "212002",
    year = "2014"
}
'
