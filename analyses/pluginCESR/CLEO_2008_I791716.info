Name: CLEO_2008_I791716
Year: 2008
Summary: Dalitz plot analysis of $D^+\to K^+K^-\pi^+$
Experiment: CLEO
Collider: CESR
InspireID: 791716
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 78 (2008) 072003
RunInfo: Any process producing D0
Description:
  'Kinematic distributions  in the decay $D^+\to \to K^+K^-\pi^+$. Resolution/acceptance effects have been not unfolded. Given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
ValidationInfo:
  'Compared Herwig events using the same model for the decay as in the paper. The data were read from the plots in the paper.
   '
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:2008msk
BibTeX: '@article{CLEO:2008msk,
    author = "Rubin, P. and others",
    collaboration = "CLEO",
    title = "{Search for CP Violation in the Dalitz-Plot Analysis of D+- ---\ensuremath{>} K+ K- pi+-}",
    eprint = "0807.4545",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CLNS-08-2036, CLEO-08-19",
    doi = "10.1103/PhysRevD.78.072003",
    journal = "Phys. Rev. D",
    volume = "78",
    pages = "072003",
    year = "2008"
}
'
