BEGIN PLOT /CLEOII_1997_I444745/d01-x01-y01
Title=$\pi^+\pi^-\pi^-\eta$ mass in $\tau\to\pi^+\pi^-\pi^-\eta\nu_\tau$ (no $f_1$)
XLabel=$m_{\pi^+\pi^-\pi^-\eta}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^-\eta}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
