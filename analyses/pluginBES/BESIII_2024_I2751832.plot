BEGIN PLOT /BESIII_2024_I2751832/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^+\pi^-\pi^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^+\pi^-\pi^0)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2751832/d01-x01-y02
Title=Cross section for $e^+e^-\to\rho\pi\to\pi^+\pi^-\pi^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\rho\pi\to\to\pi^+\pi^-\pi^0)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2751832/d01-x01-y03
Title=Cross section for $e^+e^-\to\rho(1450)\pi\to\pi^+\pi^-\pi^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\rho(1450)\pi\to\to\pi^+\pi^-\pi^0)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
