BEGIN PLOT /BESIII_2012_I1187787/d01-x01-y02
Title=$K^0_SK^\pm$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_SK^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1187787/d01-x01-y03
Title=$K^\pm\pi^\mp$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1187787/d01-x01-y01
Title=$K^0_S\pi^\pm$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1187787/dalitz
Title=Dalitz plot for  $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^\pm\pi^\mp}/{\rm d}m^2_{K^0_S\pi^\pm}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT