BEGIN PLOT /BABAR_2018_I1679886/d01-x01-y01
Title=$K^-K^0_S$ mass in $\tau^-\to K^-K^0_S\nu_\tau$ decays
XLabel=$m_{K^-K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^-K^0_S}$ [ $\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2018_I1679886/d01-x01-y02
Title=Spectral function in $\tau^-\to K^-K^0_S\nu_\tau$ decays
XLabel=$m_{K^-K^0_S}$ [$\mathrm{GeV}$]
YLabel=$V(q)\times1000$
LogY=0
END PLOT
