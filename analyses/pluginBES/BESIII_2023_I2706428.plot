BEGIN PLOT /BESIII_2023_I2706428/d01-x01-y01
Title=Cross section for $e^+e^-\to\eta J/\psi$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta J/\psi)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
