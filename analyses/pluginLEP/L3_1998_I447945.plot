BEGIN PLOT /L3_1998_I447945/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta^\prime$
XLabel=$Q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q^2$ [pb]
END PLOT
