BEGIN PLOT /BELLE_2006_I735859/d01
XLabel=$|\cos\theta|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta|$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I735859/d01-x01-y01
Title=Helicity angle in $B^+\to\omega K^+$
END PLOT
BEGIN PLOT /BELLE_2006_I735859/d01-x01-y02
Title=Helicity angle in $B^+\to\omega \pi^+$
END PLOT
BEGIN PLOT /BELLE_2006_I735859/d01-x01-y03
Title=Helicity angle in $B^0\to\omega K^0$
END PLOT

