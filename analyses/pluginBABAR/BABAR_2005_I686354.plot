BEGIN PLOT /BABAR_2005_I686354/d01-x01-y01
Title=$\pi^+\pi^-$ mass for $\psi(4230)\to \pi^+\pi^-J/\psi$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
