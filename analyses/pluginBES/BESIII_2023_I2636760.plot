BEGIN PLOT /BESIII_2023_I2636760/d01-x01-y01
Title=$\alpha_\gamma$ for $\Sigma^+\to p \gamma$
YLabel=$\alpha_\gamma$
LogY=0
YMax=-0.2
YMin=-1
END PLOT
BEGIN PLOT /BESIII_2023_I2636760/d01-x01-y02
Title=$\alpha_\gamma$ for $\bar\Sigma^-\to \bar{p} \gamma$
YLabel=$\alpha_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2636760/d01-x01-y03
Title=$\alpha_\gamma$ for $\Sigma^+\to p \gamma$, $\bar\Sigma^-\to \bar{p} \gamma$
YLabel=$\alpha_\gamma$
LogY=0
YMax=-0.2
YMin=-1
END PLOT
