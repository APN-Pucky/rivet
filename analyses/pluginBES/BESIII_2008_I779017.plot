BEGIN PLOT /BESIII_2008_I779017/d01-x01-y01
Title=Non-$D\bar{D}$ hadronic cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [GeV]
LogY=1
ConnectGaps=1
END PLOT
