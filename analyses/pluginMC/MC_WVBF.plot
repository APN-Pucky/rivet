# BEGIN PLOT /MC_WVBF/W_jet1_dR
Title=Separation between W boson and leading jet
XLabel=$\Delta R(\mathrm{W,~1st~jet})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta R(\mathrm{W,~1st~jet})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/W_jet1_deta
Title=Separation between Z boson and leading jet
XLabel=$\Delta{\eta}(\mathrm{W,~1st~jet})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta{\eta}(\mathrm{W,~1st~jet})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jets_deta_
Title=Pseudorapidity separation between jets
# END PLOT

# BEGIN PLOT /MC_WVBF/jets_dphi_
Title=Pseudorapidity separation between jets
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_eta_1
Title=Pseudorapidity of leading jet
XLabel=$\eta(\mathrm{jet~1})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta(\mathrm{jet~1})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_eta_2
Title=Pseudorapidity of second jet
XLabel=$\eta(\mathrm{jet~2})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta(\mathrm{jet~2})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_eta_3
Title=Pseudorapidity of third jet
XLabel=$\eta(\mathrm{jet~3})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta(\mathrm{jet~3})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_eta_4
Title=Pseudorapidity of fourth jet
XLabel=$\eta(\mathrm{jet~4})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta(\mathrm{jet~4})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_y_1
Title=Rapidity of first jet
XLabel=$y(\mathrm{jet~1})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y(\mathrm{jet~1})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_y_2
Title=Rapidity of second jet
XLabel=$y(\mathrm{jet~2})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y(\mathrm{jet~2})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_y_3
Title=Rapidity of third jet
XLabel=$y(\mathrm{jet~3})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y(\mathrm{jet~3})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_y_4
Title=Rapidity of fourth jet
XLabel=$y(\mathrm{jet~4})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y(\mathrm{jet~4})$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/N_gapjets_exclusive
Title=Exclusive gap-jet multiplicity
XLabel=$N_{\mathrm{jet}}$
YLabel=$\sigma(N_{\mathrm{jet}})$ [fb]
XMinorTickMarks=0
ErrorBands=1
# END PLOT

# BEGIN PLOT /MC_WVBF/N_gapjets_inclusive
Title=Inclusive gap-jet multiplicity
XLabel=$N_{\mathrm{jet}}$
YLabel=$\sigma(\geq N_{\mathrm{jet}})$ [fb]
XMinorTickMarks=0
ErrorBands=1
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_pT_1
Title=Transverse momentum of leading jet
XLabel=$p_\perp(\mathrm{jet~1})$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp(\mathrm{jet~1})$ [fb/GeV]
LogX=1
XMin=20.0
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_pT_2
Title=Transverse momentum of second jet
XLabel=$p_\perp(\mathrm{jet~2})$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp(\mathrm{jet~2})$ [fb/GeV]
LogX=1
XMin=20.0
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_pT_3
Title=Transverse momentum of third jet
XLabel=$p_\perp(\mathrm{jet~3})$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp(\mathrm{jet~3})$ [fb/GeV]
LogX=1
XMin=20.0
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_pT_4
Title=Transverse momentum of fourth jet
XLabel=$p_\perp(\mathrm{jet~4})$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp(\mathrm{jet~4})$ [fb/GeV]
LogX=1
XMin=20.0
# END PLOT

# BEGIN PLOT /MC_WVBF/jets_HT
Title=Scalar sum of jet transverse momenta ($H_\mathrm{T}$)
XLabel=$H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}H_\mathrm{T}$ [fb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_WVBF/m_jj
Title=Dijet invariant mass spectrum
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}m_{jj}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_WVBF/dphi_jj
Title=Dijet azimuthal difference
XLabel=$\Delta \phi(j,j)$ [rad$/\pi$] 
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta \phi$ [fb/rad]
# END PLOT

# BEGIN PLOT /MC_WVBF/drap_jj
Title=Dijet rapidity difference
XLabel=$\Delta y(j,j)$ 
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta y$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/jet_3_centrality
Title=
XLabel=third-jet centrality ($C_j$)
YLabel=$\mathrm{d}\sigma/\mathrm{d}C_j$ [fb]
# END PLOT

# BEGIN PLOT /MC_WVBF/W_pT
Title=W $p_\perp$
XLabel=$p_\perp^{\mathrm{W}}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp^{\mathrm{W}}$ [fb/GeV]
LogX=1
# END PLOT

