BEGIN PLOT /BELLE_2018_I1672149/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \eta^\prime f_2(1270)$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1672149/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \eta^\prime \pi^+\pi^-$ no ($f_2\eta^\prime$ or $\eta_c$)
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
