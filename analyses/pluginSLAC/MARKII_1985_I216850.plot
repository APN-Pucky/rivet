BEGIN PLOT /MARKII_1985_I216850/d01-x01-y01
Title=Scaled Charged Particle Momenta  in symmetric 3-jet event
XLabel=$x$
YLabel=$\frac1{N_{\text{jet}}} \text{d}n_{\text{charged}}/\text{d}x$
END PLOT
