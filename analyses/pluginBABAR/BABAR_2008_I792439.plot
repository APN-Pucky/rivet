BEGIN PLOT /BABAR_2008_I792439/d02-x01-y01
Title=$K^\pm\pi^\mp$ mass in $B^0\to K^\pm\pi^\mp\phi$
XLabel=$m_{K^\pm\pi^\mp}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d02-x01-y02
Title=$\phi\to K^+K^-$ mass in $B^0\to K^\pm\pi^\mp\phi$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2008_I792439/d02-x02-y01
Title=$K^0_S\pi^0$ mass in $B^0\to K^0_S\pi^0\phi$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d02-x02-y02
Title=$\phi\to K^+K^-$ mass in $B^0\to K^0_S\pi^0\phi$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2008_I792439/d03-x01-y01
Title=$K\pi$ helicity angle in $B\to\phi K\pi$ ($0.75<m_{K\pi}<1.05\,$GeV)
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_1$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d03-x01-y02
Title=$\phi$ helicity angle in $B\to\phi K\pi$ ($0.75<m_{K\pi}<1.05\,$GeV)
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_2$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d04-x01-y01
Title=$K\pi$ helicity angle in $B\to\phi K\pi$ ($1.13<m_{K\pi}<1.53\,$GeV)
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_1$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d04-x01-y02
Title=$\phi$ helicity angle in $B\to\phi K\pi$ ($1.13<m_{K\pi}<1.53\,$GeV)
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_2$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2008_I792439/d01-x02-y02
Title=Longitudinal Polarization for $B\to\phi K^*$
YLabel=$f_L$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I792439/d01-x02-y03
Title=Longitudinal Polarization for $B\to\phi K^*_2$
YLabel=$f_L$
LogY=0
END PLOT
