BEGIN PLOT /BESIII_2023_I2677290/d01-x01-y01
Title=Cross section for $e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-)$ [pb]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2677290/d01-x01-y02
Title=$|G_{\text{eff}}|$ for $e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$|G_{\text{eff}}|$ [$\times10^2$]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2677290/d01-x01-y03
Title=$\alpha$ for $e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\alpha_{\Lambda_c}$
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2677290/d01-x01-y04
Title=$|G_E/G_M|$ for $e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$|G_E/G_M|$
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2677290/d01-x01-y05
Title=$|G_{M}|$ for $e^+e^-\to \Lambda_c^+\bar{\Lambda}_c^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$|G_{M}|$ [$\times10^2$]
ConnectGaps=1
END PLOT
