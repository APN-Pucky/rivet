BEGIN PLOT /BABAR_2008_I789011/d01-x01-y01
Title=$\rho^+$ helicity angle in $e^+e^-\to\rho^+\rho^-$
XLabel=$\cos\theta_+$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_+$
END PLOT
BEGIN PLOT /BABAR_2008_I789011/d01-x01-y02
Title=$\rho^-$ helicity angle in $e^+e^-\to\rho^+\rho^-$
XLabel=$\cos\theta_-$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_-$
END PLOT
BEGIN PLOT /BABAR_2008_I789011/d01-x01-y03
Title=$\pi^+$ azimuthal angle in $e^+e^-\to\rho^+\rho^-$
XLabel=$\phi_+$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\phi_+$
END PLOT
BEGIN PLOT /BABAR_2008_I789011/d01-x01-y04
Title=$\pi^+$ azimuthal angle in $e^+e^-\to\rho^+\rho^-$
XLabel=$\phi_+$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\phi_+$
END PLOT
BEGIN PLOT /BABAR_2008_I789011/d01-x01-y05
Title=Scattering angle in $e^+e^-\to\rho^+\rho^-$
XLabel=$\cos\theta^*$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta^*$
END PLOT
